function [ phi ] = fRotationInvariantMoments( mask )
%FCOMPLEXMOMENTS Summary of this function goes 
%   Detailed explanation goes here

[x,y,pixelValues] = find(mask);
pixelValues = double(pixelValues);

m00 = sum(pixelValues);
x = x - sum(x.*pixelValues)/m00;
y = y - sum(y.*pixelValues)/m00;

c11 = fComplexMoment(x, y, pixelValues, 1, 1);
c20 = fComplexMoment(x, y, pixelValues, 2, 0);
c12 = fComplexMoment(x, y, pixelValues, 1, 2);
c21 = fComplexMoment(x, y, pixelValues, 2, 1);
c30 = fComplexMoment(x, y, pixelValues, 3, 0);
c22 = fComplexMoment(x, y, pixelValues, 2, 2);
c31 = fComplexMoment(x, y, pixelValues, 3, 1);
c40 = fComplexMoment(x, y, pixelValues, 4, 0);

phi(1) = c11;
phi(2) = c21.*c12;
phi(3) = real(c20.*c12.^2);
phi(4) = imag(c20.*c12.^2);
phi(5) = real(c30.*c12.^3);
phi(6) = imag(c30.*c12.^3);

phi(7) = c22;
phi(8) = real(c31.*c12.^2);
phi(9) = imag(c31.*c12.^2);
phi(10) = real(c40.*c12.^4);
phi(11) = imag(c40.*c12.^4);

end

