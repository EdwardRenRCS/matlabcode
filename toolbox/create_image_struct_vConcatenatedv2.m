function file_image = create_image_struct_vConcatenatedv2(image_lever_path)

%- This function will read the images in the specified folder and output a
%- structure with added info like frame and channel


path = cd; 
cd(image_lever_path);


file_image = dir('*.tif'); %- depends on what kind of images you have

cd(path);




for i_image = 1:length(file_image)
    
    
    
    name_temp    = file_image(i_image).name ;    
    channel_find =  regexp(name_temp, 'T[1234567890]+_C','end');
   
    frame_find =  regexp(name_temp, 'T[1234567890]+','end');

    frame_find   =  strfind(name_temp, 'T');
    frame        =  name_temp(frame_find+1 : channel_find-2);
    
    well = name_temp(frame_find-4:frame_find-2);

    channel      = str2num(name_temp(channel_find +1:channel_find +2));
    
    file_image(i_image).channel = channel  ;    
    file_image(i_image).frame   = str2num(frame) ; 
    file_image(i_image).well = well;


end
