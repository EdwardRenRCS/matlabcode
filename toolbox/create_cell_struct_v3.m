function [movie_struct CONSTANTS] = create_cell_struct_v3(SERVER,leverFile)

options = weboptions;
options.Timeout = 30000;
url=[SERVER leverFile '/constants'];
CONSTANTS=webread(url,options);



% for time=1:CONSTANTS.imageData.NumberOfFrames
for time=1:3
    time
    cell_struct = [] ;
    %- Get the data from the server
    url       =  [SERVER leverFile '/cellCentroids/' num2str(time)];
    tCells    =   webread(url,options);
    
    url       =  [SERVER leverFile '/cells/' num2str(time)];
    data      =  webread(url,options);
    dum       = strsplit(data,'cellID');
    
    n_cell = length(strfind(data,'trackID'));
    
    
    for i=1:n_cell
        
        
        surf_find          = strfind(dum{1,i+1},'surface');
        surf_end_find      = strfind(dum{1,i+1},'jsColor');
        track_find         = strfind(dum{1,i+1},'trackID');
        centroid_find      = strfind(dum{1,i+1},'centroid');
        
        trackID            = str2double(dum{1,i+1}(track_find+9:centroid_find-3));
        centroid           = str2num(dum{1,i+1}(centroid_find+11:surf_find-4));
        surface_vector     = dum{1,i+1}(surf_find+10:surf_end_find-4);
        num_surface        = str2num(surface_vector);
        surface            = transpose(reshape(transpose(num_surface),2,length(num_surface)/2));
        
        
        
        cell_struct(i).centroid = centroid;
        cell_struct(i).ID       = trackID;
        cell_struct(i).surface  = surface;
        
        cell_struct(i).time = time;
    end
    
    movie_struct{time} = cell_struct ;
    
    
end