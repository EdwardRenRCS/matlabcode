function [ moment ] = fComplexMoment( x, y, val, p, q )
%FCOMPLEXMOMENT Summary of this function goes here
%   Detailed explanation goes here

moment = sum((x + 1i*y).^p .* (x - 1i*y).^q .* val);
m00 = sum(val);
moment = moment/(m00^(1+(p+q)/2));
end

