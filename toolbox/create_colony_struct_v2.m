function [ colony colony_struct] = create_colony_struct_v1(movie_struct, CONSTANTS)


colony_struct    = {};
Colony_ID_tot    = [] ;

for frame = 1:CONSTANTS.imageData.NumberOfFrames
    if isempty(movie_struct{1,frame})
        disp('Empty Frame');
        continue
    end
    
    Colony_ID     = [movie_struct{1,frame}.colony_ID];
    Colony_ID_tot = [Colony_ID_tot Colony_ID ];
    
end

N_colony = max(Colony_ID_tot) - 1 ;

colony   = unique(Colony_ID_tot);
colony   = colony(2:end);
colony_struct =  [] ;

for i_colony = 1:length(colony)
    
    
    
    i_colony/length(colony)
    for i_frame = 1:CONSTANTS.imageData.NumberOfFrames;
        
        if isempty(movie_struct{1,i_frame})
            continue
        end
        
        colony_ID                    = [movie_struct{1,i_frame}.colony_ID];
        
        ind_colony                   = colony_ID == colony(i_colony);
        
        colony_struct{i_colony,i_frame} = movie_struct{1,i_frame}(ind_colony);
        
        
    end
    
end
end
