function [ moment ] = fMoment( x, y, val, p, q )
%FCOMPLEXMOMENT Summary of this function goes here
%   Detailed explanation goes here

moment = sum(x.^p .* y.^q .* val);
m00 = sum(val);
moment = moment/(m00^(1+(p+q)/2));
end

