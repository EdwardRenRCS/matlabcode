function file_image = create_image_struct_v1(image_lever_path)

%- This function will read the images in the specified folder and output a
%- structure with added info like frame and channel


path = cd; 
cd(image_lever_path);


file_image = dir('*.tiff'); %- depends on what kind of images you have

    file_image = file_image(logical(cellfun(@(x) ~isempty(strfind(x,'well')), {file_image.name})));

cd(path);




for i_image = 1:length(file_image)
    
    
    
    name_temp    = file_image(i_image).name ;    
    channel_find =  strfind(name_temp, '_c');
   

    frame_find   =  strfind(name_temp, '_t');
    frame        =  name_temp(frame_find+2 : frame_find +5);

    channel      = str2num(name_temp(channel_find +2:frame_find-1));
    
    file_image(i_image).channel = channel  ;    
    file_image(i_image).frame   = str2num(frame) ; 



end
