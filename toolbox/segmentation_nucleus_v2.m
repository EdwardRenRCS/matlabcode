function [segmentation_mask] = segmentation_nucleus_v2(image, param)

%% Thresholding step 


switch param.method
    
    case 'global_threshold'

        if isempty(param.otsu_coeff)
            param.otsu_coeff = 1; 
        end
        if isempty(param.n_class)
            param.n_class = 2; 
        end
        if isempty(param.otsu_thresh_index)
            param.n_class = 1; 
        end
        
thresh_otsu = param.otsu_coeff*multithresh(image,param.n_class - 1);
im_binarize = image > thresh_otsu(param.otsu_thresh_index);

    case 'local_threshold'


% im_filt     = medfilt2(image,[8 8]); % median filtering
im_filt       = averagefilter(image, [5 5], 'symmetric'); % median filtering
im_filtered   = averagefilter(im_filt, [50 50], 'symmetric'); % mean filter to estimate bgd


im_bgd_reduced                     = im_filt - im_filtered; %remove background from image
im_bgd_reduced(im_bgd_reduced < 0) = 0; %set the negative pixels to 0

threshold_local                    = param.threshold_local; % threshold the image
im_binarize                        = im_bgd_reduced > threshold_local; % binarize the image, the image is now 0 for bgd and 1 for signal (nucleus)

end

%% Separate object



binary_mask           = bwareaopen(im_binarize,param.size_threshold); % Remove small object

distance              = bwdist(1 - binary_mask); %%% binary_mask is your binarized image of your nuclei. 
%%% MATLAB computes the distance from 0 pixels to non-zero pixels. This why
%%% I did 1 - binary_mask, we want the cells to be 0 and the bgd to be 1 to
%%% use that function. 

% figure
% imshow(distance,[]); % To take a look at it.
% 

%%% We will now modify the distance transform so that we only detect the
%%%  local maxima with amplitude bigger than h.

h      = param.h_maxima; % set the value of h --> too small oversegmentation , too large under segmentation
marker = distance - h; % we shift the image
IM     = imreconstruct(marker,distance); % reconstruct it with morphological operator
im_h   = distance - IM; % Take the difference between original and reconstructed

region_local_max         = zeros(size(marker,1), size(marker,2));
region_local_max(im_h>0) = 1; % local maxima region are the non zero value

% figure
% imshow(region_local_max,[]) % take a look at the maxima region


D2 = imimposemin(-distance,region_local_max); %modify the distance map so that only have the previously detected maxima


b         = watershed(D2); %Apply the watershed
im_result = double(b).*double(binary_mask); %Extract the watershed region only where there is signal


size_threshold = param.size_threshold;  %min size of your nuclei
im_result      = bwareaopen(im_result,size_threshold); % remove the too smaller than size_threshold



im_result          = bwconncomp(im_result);  %
segmentation_mask  = labelmatrix(im_result); % Output the results as a label matrix instead of binary (optional)
segmentation_mask  =  imfill(segmentation_mask ,'holes');
a                  = mask2poly(segmentation_mask);


if param.show 
figure
imshow(image,[])
hold all
plot(a(:,1), a(:,2),'.','col','red')
end


