function file_image = create_image_struct_vAndyData(image_lever_path)

%- This function will read the images in the specified folder and output a
%- structure with added info like frame and channel

filesList = dir(image_lever_path)
% Get a logical vector that tells which is a directory.
dirFlags = [filesList.isdir]
% Extract only those that are directories.
folder_well = filesList(dirFlags)

folder_well = folder_well(~ismember({folder_well(:).name},{'.','..'}))
file_image = []
for i_well = 1:length(folder_well)

cd(fullfile(folder_well(i_well).folder,folder_well(i_well).name))

file_image_well  = dir(fullfile('*.tif'));
file_image_well = file_image_well(cellfun(@(x) strcmp('Assay',x(1:5)), {file_image_well.name}));

for i_image = 1:length(file_image_well)
    
    
    fname = file_image_well(i_image).name ;

%     day=regexp(folder_well{i_well},'-D(\d)','tokens','once');
%     day = str2double(day);
    day= i_well;
    
    channel=regexp(fname,'C(\d{2,}).tif','tokens','once');
    numchannel=str2double(channel);
    
    time=regexp(fname,'_T(\d{4,})','tokens','once');
    numtime=str2double(time);
    
    field=regexp(fname,'F(\d{3,})L','tokens','once');
    numfield=str2double(field);
    
    well=regexp(fname,'_(...)_T','tokens','once');

 day

file_image_well(i_image).channel = channel ;  
file_image_well(i_image).time    = time ;  
file_image_well(i_image).well    = well ;  
file_image_well(i_image).field   = field ;  
file_image_well(i_image).day     = day ;

file_image_well(i_image).channel = numchannel ;  
file_image_well(i_image).time    = numtime ;  
file_image_well(i_image).field   = numfield ;  

end
file_image = [file_image ; file_image_well ];

end


table_files = struct2table(file_image);
for day=2:max(table_files.day)
    table_files{(table_files.day==day),'time'}=table_files{(table_files.day==day),'time'}...
        +max(table_files{(table_files.day<day),'time'});
end

file_image = table_files;
