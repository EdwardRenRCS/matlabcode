# README #
This module requires the MatLab Image Processing Toolbox.

### What is this repository for? ###

This repository is for pulling segmentation results from LEVER and extracting numerical features from the resulting segmented cells.

### How do I get set up? ###

Add all folders to the MatLab search path.
Use FeatureExtraction\ExtractingCellFeatures.m to extract features.
Ensure that it is pulls segmentations from the correct LEVER file and the segments line up with the correct microscopy images.
The amount of parallel processing the script utilises can be modified.
Ensure the write path for the SQ Lite file is easily accessible.
