%%%% SCRIPT TO DO MITOSIS DETECTION USING LEVER RESULTS


%% Open the LEVER results


LEVER_STRUCT.server      = 'https://leverjs.net/RafaelH2B';   
LEVER_STRUCT.lever_file  = '/well_E03_field_F001.LEVER';
LEVER_STRUCT.lever_path  = '/Users/as17351/Documents/DATA/data_3_colors/20180521F+HJF+50ms_20180521_152618/AssayPlate_PerkinElmer_CellCarrier-96/well_E03_field_F001'

SERVER           = 'https://leverjs.net/RafaelH2B';
leverFile        = '/well_E03_field_F001.LEVER';
lever_path  = '/Users/as17351/Documents/DATA/data_3_colors/20180521F+HJF+50ms_20180521_152618/AssayPlate_PerkinElmer_CellCarrier-96/well_E03_field_F001'


%- Create the cell struct

[movie_struct CONSTANTS] = create_cell_struct_v2(SERVER,leverFile);


path = cd; 
cd(lever_path);
file_image = dir('*.tiff');
cd(path);


for i_image = 1:length(file_image)
    
    
    
    name_temp    = file_image(i_image).name ;    
    channel_find =  strfind(name_temp, '_c');
    channel      = name_temp(channel_find +2);

    frame_find   =  strfind(name_temp, '_t');
    frame        =  name_temp(frame_find+2 : frame_find +5);

    file_image(i_image).channel = str2num(channel)  ;    
    file_image(i_image).frame   = str2num(frame) ; 



end


%% Load model for mitosis classification 

[feature_training_name feature_training_path ] = uigetfile('*.csv')
feature_training      = readtable(fullfile(feature_training_path,feature_training_name));

t           = templateSVM('Standardize',1);
Mdl         = fitcecoc(feature_training,'mitotic_phase','Learners',t,'FitPosterior',1);

%% Do classification based on the homemade segmentation

frame          = 1;
size_image_H2B = [2160 2560]; 


image_file_select = file_image([file_image.channel] == 1)
image_file_select = image_file_select([image_file_select.frame] == frame)

image_H2B = imread(fullfile(image_file_select.folder,image_file_select.name)); 


param.h_maxima          = 5;
param.otsu_thresh_index = 1;
param.otsu_coeff        = 1;
param.size_threshold    = 200;
param.method            = 'local_threshold'
param.n_class           = 2; 
param.show              = 0;
param.threshold_local   = 3; 
param.min_size_colony   = 10; 
param.DICE_threshold    = 0.5;
  
seg         = segmentation_nucleus_v2(image_H2B,param);


CC  = bwconncomp(seg);
reg = regionprops(CC,'PixelList','PixelIdxList','Centroid')
feature_cell_tot = [] ; 
for i_cell = 1:CC.NumObjects
    
   disp(strcat('processing cell n',num2str(i_cell),{' '},'out of',{' '},num2str(CC.NumObjects))) 
    
   cell_pos   = reg(i_cell).PixelList;
         
   
   min_x = min(cell_pos(:,1));  
   min_y = min(cell_pos(:,2));
   max_x = max(cell_pos(:,1));
   max_y = max(cell_pos(:,2));

[X,Y]            = meshgrid(min_x:max_x,min_y:max_y);
ind_inside_cell  = double(inpolygon(X,Y,cell_pos(:,1), cell_pos(:,2)));

im_crop    = image_H2B(min_y:max_y,min_x:max_x);

    
   
centroid_x = mean(reg(i_cell).PixelList(:,1));
centroid_y = mean(reg(i_cell).PixelList(:,2));
   
    
    
feature_cell              = mitosis_feature_calculation_v1(im_crop,ind_inside_cell);
mitotic_phase             = predict(Mdl,struct2array(feature_cell));


reg(i_cell).mitotic_phase = mitotic_phase;

% feature_cell.mitotic_phase = mitotic_phase;
% feature_cell.cell_ID       = i_cell;
% feature_cell.file_ID       = fullfile(handles.folder_image,handles.image_name);
% feature_cell.centroid      = [centroid_x centroid_y];
% handles.feature_cell(i_cell)         = feature_cell;



feature_cell_tot(i_cell).cell_ID       = i_cell;
feature_cell_tot(i_cell).mitotic_phase = mitotic_phase;
feature_cell_tot(i_cell).cell_ID       = i_cell;
feature_cell_tot(i_cell).centroid      = [centroid_x centroid_y];


end


%% Assign to lever segmentation

ind_mitotic_condensed     = [feature_cell_tot.mitotic_phase] == 2; 

feature_mitotic_condensed = feature_cell_tot(ind_mitotic_condensed)
cell_ID_mitosis = [] ; 

for i_cell_mitosis = 1:length(feature_mitotic_condensed)


   
 frame_struct     =  movie_struct{1,frame};   
 ind_cell = []; 
 
 
 for i_cell_LEVER = 1:length(frame_struct)
     
     
 ind_cell(i_cell_LEVER) = inpolygon(feature_mitotic_condensed(i_cell_mitosis).centroid(1), feature_mitotic_condensed(i_cell_mitosis).centroid(2),  frame_struct(i_cell_LEVER).surface(:,1),frame_struct(i_cell_LEVER).surface(:,2))
      
 end
 
 
 cell_ID_mitosis(i_cell_mitosis) = frame_struct(find(ind_cell == 1)).ID;
 
 
end






 
 feature_mitotic_condensed(i_cell_mitosis).
 
 
 
 
 
 
 
 figure
 plot(feature_mitotic_condensed(i_cell_mitosis).centroid(1), feature_mitotic_condensed(i_cell_mitosis).centroid(2),'.','MarkerSize',20,'col','red')
 for i= 1:length(frame_struct)
 hold all
 plot(frame_struct(i).surface(:,1), frame_struct(i).surface(:,2),'.')
 end
 hold all
 plot(frame_struct(4).surface(:,1), frame_struct(4).surface(:,2),'+')
 hold all
 plot(frame_struct(20).surface(:,1), frame_struct(20).surface(:,2),'+')
 
 
 
 
 
 
 
 
 
 
 

  mask_cell    = zeros(size(image_H2B,1), size(image_H2B,2));  
  mask_cell    = poly2mask(cell_pos(:,1), cell_pos(:,2),size_image_H2B(1), size_image_H2B(2));   
  feature_cell = mitosis_feature_calculation_v1(image_H2B,mask_cell);
    
    
    
    
                

 movie_struct{1,frame}(i_cell).mitotic_phase = predict(Mdl,struct2array(feature_cell));



















end




























figure
imshow(image_H2B,[0 650])

for i_cell = 1:CC.NumObjects
    
    hold all
    
    id_cell = feature_cell_tot(i_cell).cell_ID; 
    
    cell_surf             = reg(id_cell).PixelList;
    ind_bound             = boundary(cell_surf(:,1), cell_surf(:,2));
    cell_surf             = cell_surf(ind_bound,:);
    
    
    
%     mask_cell = zeros(size(image_H2B,1), size(image_H2B,2));
%     mask_cell(reg(i_cell).PixelIdxList) = 1; 
%     a = mask2poly(mask_cell);
    
% mitotic_phase = str2num(feature_cell_tot(i_cell).mitotic_phase{1});
 mitotic_phase = feature_cell_tot(i_cell).mitotic_phase;


    if mitotic_phase == 1
        
        plot(cell_surf(:,1),cell_surf(:,2),'.','col','red') 
        
    elseif mitotic_phase == 2
        
        plot(cell_surf(:,1),cell_surf(:,2),'.','col','green') 
        
    elseif  mitotic_phase == 3

       plot(cell_surf(:,1),cell_surf(:,2),'.','col','yellow')
       
    elseif  mitotic_phase == 4

       plot(cell_surf(:,1),cell_surf(:,2),'.','col','blue') 
       
    end


end













for i_cell = 1:length(movie_struct{1,frame})
    
 frame_struct     =  movie_struct{1,frame}
    
 cell_pos         =   frame_struct.surface
    
      
  
  

  
    mask_cell    = zeros(size(image_H2B,1), size(image_H2B,2));  
    mask_cell    = poly2mask(cell_pos(:,1), cell_pos(:,2),size_image_H2B(1), size_image_H2B(2));   
    feature_cell = mitosis_feature_calculation_v1(image_H2B,mask_cell);
    
    
    
    
                

 movie_struct{1,frame}(i_cell).mitotic_phase = predict(Mdl,struct2array(feature_cell));

 
 



end






figure
imshow(image_H2B,[0 650])

for i_cell = 1:length(movie_struct{1,frame})
    
    
    hold all
    
    
    cell_surf             = movie_struct{1,frame}(i_cell).surface;
    
    
    
%     mask_cell = zeros(size(image_H2B,1), size(image_H2B,2));
%     mask_cell(reg(i_cell).PixelIdxList) = 1; 
%     a = mask2poly(mask_cell);
    
mitotic_phase = movie_struct{1,frame}(i_cell).mitotic_phase;


    if mitotic_phase == 1
        
        plot(cell_surf(:,1),cell_surf(:,2),'.','col','red') 
        
    elseif mitotic_phase == 2
        
        plot(cell_surf(:,1),cell_surf(:,2),'.','col','green') 
        
    elseif  mitotic_phase == 3

       plot(cell_surf(:,1),cell_surf(:,2),'.','col','yellow')
       
    elseif  mitotic_phase == 4

       plot(cell_surf(:,1),cell_surf(:,2),'.','col','blue') 
       
    end


end





