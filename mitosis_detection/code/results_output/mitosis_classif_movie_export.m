%%%% Script to do mitosis detection and export the movie

%% Get the images

file_image       = dir('*.tiff')

for i_image = 1:length(file_image)
    
    
    fname = file_image(i_image).name
    
    [cStart]=regexp(fname,'_c\d+');
    [tStart]=regexp(fname,'_t\d+');
    [zStart]=regexp(fname,'_z\d+');
    
    channel = str2num(fname(cStart+2))
    time    = str2num(fname(tStart+2:zStart-1))
    
    file_image(i_image).channel = channel ;
    file_image(i_image).time = time ;
    
end


channel_H2B = 1;

file_image_H2B = file_image([file_image.channel] == channel_H2B)

%% Train the model

%- Random Forest classifier
training_set = readtable('training_set_v2.csv');


Mdl          = TreeBagger(500,table2array(training_set(:,1:end-1)),table2array(training_set(:,end)),'NumPredictorsToSample',9,'OOBPrediction','on','InBagFraction',0.3)

t           = templateSVM('Standardize',1);
Mdl = fitcecoc(training_set,'mitotic_phase','Learners',t,'FitPosterior',1);

%% Do the seg and detection


output_folder = ''

if ~isdir(output_folder)
    mkdir(output_folder)
end



param.h_maxima          = 5; %- param for the segmentation
param.otsu_thresh_index = 1;
param.otsu_coeff        = 1;
param.size_threshold    = 200;
param.method            = 'local_threshold'
param.n_class           = 2;
param.show              = 0;
param.threshold_local   = 3;
param.min_size_colony   = 10;
param.DICE_threshold    = 0.5;



for i_image = 1:length(file_image_H2B)
    
    
    image_H2B                = imread(fullfile(file_image_H2B(i_image).folder,file_image_H2B(i_image).name));
    
    
    
    seg         = segmentation_nucleus_v2(image_H2B,param); %- perform the segmentation
    
    
    CC  = bwconncomp(seg);
    reg = regionprops(CC,'PixelList','PixelIdxList','Centroid') %- Get info for every cells
    feature_cell_tot = [] ;
    
    for i_cell = 1:CC.NumObjects %- Loop over every cells
        
        disp(strcat('processing cell n',num2str(i_cell),{' '},'out of',{' '},num2str(CC.NumObjects))) ;
        
        mask_cell = zeros(size(image_H2B,1), size(image_H2B,2));
        mask_cell(reg(i_cell).PixelIdxList) = 1; %-
        
             
        
        centroid_x = mean(reg(i_cell).PixelList(:,1));
        centroid_y = mean(reg(i_cell).PixelList(:,2)); %- DUMB
        
        
        
        feature_cell              = mitosis_feature_calculation_v1(image_H2B,mask_cell);
        mitotic_phase             = predict(Mdl,struct2array(feature_cell));
        reg(i_cell).mitotic_phase = mitotic_phase;
        
        
        
        feature_cell_tot(i_cell).cell_ID       = i_cell;
        feature_cell_tot(i_cell).mitotic_phase = mitotic_phase;
        feature_cell_tot(i_cell).centroid      = [centroid_x centroid_y];
        
        
    end
    
    %% Plot the results
    
    figure('visible','off')
    imshow(image_H2B,[])
    
    for i_cell = 1:size(feature_cell_tot,2)
        
        hold all
        
        id_cell = feature_cell_tot(i_cell).cell_ID;
        
        cell_surf             = reg(id_cell).PixelList;
        ind_bound             = boundary(cell_surf(:,1), cell_surf(:,2));
        cell_surf             = cell_surf(ind_bound,:);
        
        
        
        mitotic_phase = feature_cell_tot(i_cell).mitotic_phase;
        
        
        if mitotic_phase == 1
            
            plot(cell_surf(:,1),cell_surf(:,2),'.','col','red')
            
        elseif mitotic_phase == 2
            
            plot(cell_surf(:,1),cell_surf(:,2),'.','col','green')
            
        elseif  mitotic_phase == 3
            
            plot(cell_surf(:,1),cell_surf(:,2),'.','col','yellow')
            
        elseif  mitotic_phase == 4
            
            plot(cell_surf(:,1),cell_surf(:,2),'.','col','blue')
            
        end
        
        
    end
    
    
    
    saveas(gcf,fullfile(output_folder,'frame_fig.png'))
    close(gcf)
    
    frame_fig = imread(fullfile(output_folder,'frame_fig.png'));
    
    imwrite(frame_fig, fullfile(output_folder,'mitosis_detect.tif'), 'WriteMode', 'append',  'Compression','none');
    delete(fullfile(output_folder,'frame_fig.png'));
    
end




