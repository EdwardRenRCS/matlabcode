function handles = plot_cell_classif_v1(handles)
%- Function in the GUI that display the classification results on the GUI. 



CC  = handles.CC;  %- Get the CC structure (seg)
reg = handles.reg; %- Get the reg structure (region props of CC)

axes(handles.axes1);

imshow(handles.image_H2B,[0 650]) %- Display the images

for i_cell = 1:size(handles.feature_cell,2)
    
    hold all
    
    
    
    id_cell = handles.feature_cell(i_cell).cell_ID; 
    
    cell_surf             = reg(id_cell).PixelList;
    ind_bound             = boundary(cell_surf(:,1), cell_surf(:,2));
    cell_surf             = cell_surf(ind_bound,:); %- Get the boundary of the cell
        

 mitotic_phase = handles.feature_cell(i_cell).mitotic_phase; %- Plot with different colors the different classes for cells

    if mitotic_phase == 1
        
        plot(cell_surf(:,1),cell_surf(:,2),'.','col','red') 
        
    elseif mitotic_phase == 2
        
        plot(cell_surf(:,1),cell_surf(:,2),'.','col','green') 
        
    elseif  mitotic_phase == 3

       plot(cell_surf(:,1),cell_surf(:,2),'.','col','yellow')
       
    elseif  mitotic_phase == 4

       plot(cell_surf(:,1),cell_surf(:,2),'.','col','blue') 
       
    end


end

hold all
plot(handles.cell_centroid(:,1), handles.cell_centroid(:,2),'+','col','red')


