function handles = train_model_v1(handles)
%- Function of the GUI to train the model, either RF or SVM.


feature_training = handles.training_set; %- Get the loaded training set 

method = handles.method.String{handles.method.Value}; %- Get the method chosen

if strcmp(method,'Random Forest')

 handles.Mdl = TreeBagger(500,table2array(feature_training(:,1:end-1)),table2array(feature_training(:,end)),'NumPredictorsToSample',9,'OOBPrediction','on','InBagFraction',0.3)

else
 
t           = templateSVM('Standardize',1);
handles.Mdl = fitcecoc(feature_training,'mitotic_phase','Learners',t,'FitPosterior',1);


end


disp('Model trained')