function handles =  annotate_cell_v1(handles)
%- Function of the GUI to annotate cells
id_cell  = handles.id_cell ; 
id_class = get(handles.listbox1,'Value'); 
handles.feature_cell(id_cell).mitotic_phase = id_class; 
