function varargout = mitosis_annotater(varargin)
% MITOSIS_ANNOTATER MATLAB code for mitosis_annotater.fig
%      MITOSIS_ANNOTATER, by itself, creates a new MITOSIS_ANNOTATER or raises the existing
%      singleton*.
%
%      H = MITOSIS_ANNOTATER returns the handle to a new MITOSIS_ANNOTATER or the handle to
%      the existing singleton*.
%
%      MITOSIS_ANNOTATER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MITOSIS_ANNOTATER.M with the given input arguments.
%
%      MITOSIS_ANNOTATER('Property','Value',...) creates a new MITOSIS_ANNOTATER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mitosis_annotater_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mitosis_annotater_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mitosis_annotater

% Last Modified by GUIDE v2.5 12-Jul-2018 15:50:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mitosis_annotater_OpeningFcn, ...
                   'gui_OutputFcn',  @mitosis_annotater_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mitosis_annotater is made visible.
function mitosis_annotater_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mitosis_annotater (see VARARGIN)

% Choose default command line output for mitosis_annotater
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes mitosis_annotater wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = mitosis_annotater_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in annotate.
function annotate_Callback(hObject, eventdata, handles)
% hObject    handle to annotate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[x, y]                  = ginput(1);
handles.group           = [x y];  %- Get the coordinate where user clicked
cell_centroid           = handles.cell_centroid; 
dist_points             = distmat(handles.group, cell_centroid);
[min_d handles.id_cell] = min(dist_points);  %- Find the closest cell to the click


handles                 = annotate_cell_v1(handles); %- update the cell class of the annotated cells
handles                 = plot_cell_classif_v1(handles); %- re do the plotting 

guidata(hObject, handles);






% --- Executes on button press in Load_image.
function Load_image_Callback(hObject, eventdata, handles)
% hObject    handle to Load_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[handles.image_name  handles.folder_image ]= uigetfile('*.tiff'); %- open a box to select the images to open


handles = segmentation_classification_v1(handles); %- we do seg and classif on the image
handles = plot_cell_classif_v1(handles);  %- we plot the results


guidata(hObject, handles);






% --- Executes on button press in Save.
function Save_Callback(hObject, eventdata, handles)
% hObject    handle to Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

folder     =   uigetdir
image_name = handles.feature_cell(1).file_ID ;
dum        = strsplit(image_name,'/');
name       = dum{end};
dum        = strsplit(name,'.');
name_table = strcat(dum{1},'_feature_table_mitosis.csv');
writetable(struct2table(handles.feature_cell),fullfile(folder,name_table)); %- save the feature table (Haralick features + mitotic class )




% --- Executes on button press in load_training.
function load_training_Callback(hObject, eventdata, handles)
% hObject    handle to load_training (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[training_set_name training_set_path] = uigetfile('*.csv'); %- open a box to chose the .csv file of the training set

handles.training_set = readtable(fullfile(training_set_path,training_set_name)); %- open it

handles = train_model_v1(handles); %- train a model based on the training set 

guidata(hObject, handles);


% --- Executes on button press in delete_cell.
function delete_cell_Callback(hObject, eventdata, handles)
% hObject    handle to delete_cell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[x, y]          = ginput(1);
handles.group   = [x y];  %- Get the coordinate where user clicked
cell_centroid   = handles.cell_centroid; 
dist_points     = distmat(handles.group, cell_centroid);
[min_d id_cell] = min(dist_points);  %- Find the closest cell to the click

handles.feature_cell(id_cell) = []; %- Delete this cell

cell_centroid = vertcat(handles.feature_cell.centroid);
cell_centroid = [cell_centroid(:,1) cell_centroid(:,2)];
handles.cell_centroid = cell_centroid; %- re update the centroid


handles         = plot_cell_classif_v1(handles);  %- re generate the plot without the deleted cell

guidata(hObject, handles);


% --- Executes on button press in train.
function train_Callback(hObject, eventdata, handles)
% hObject    handle to train (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = train_model_v1(handles);
handles = segmentation_classification_v1(handles);
handles = plot_cell_classif_v1(handles); 

guidata(hObject, handles);




% --- Executes on selection change in method.
function method_Callback(hObject, eventdata, handles)
% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from method

handles = train_model_v1(handles);
guidata(hObject, handles);




% --- Executes during object creation, after setting all properties.
function method_CreateFcn(hObject, eventdata, handles)
% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
