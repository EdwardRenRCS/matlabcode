function handles = segmentation_classification_v1(handles)
%- This is the function used in the GUI to perform segmentation and
%  classification of cells


image_H2B = imread(fullfile(handles.folder_image,handles.image_name)); 
handles.image_H2B = image_H2B; 

param.h_maxima          = 5;
param.otsu_thresh_index = 1;
param.otsu_coeff        = 1;
param.size_threshold    = 200;
param.method            = 'local_threshold'
param.n_class           = 2; 
param.show              = 0;
param.threshold_local   = 3; 
param.min_size_colony   = 10; 
param.DICE_threshold    = 0.5;



seg         = segmentation_nucleus_v2(image_H2B,param);
handles.seg = seg;



CC  = bwconncomp(seg);
reg = regionprops(CC,'PixelList','PixelIdxList','Centroid')

for i_cell = 1:CC.NumObjects
    
    disp(strcat('processing cell n',num2str(i_cell),{' '},'out of',{' '},num2str(CC.NumObjects)))
    
    

    
    cell_pos   = reg(i_cell).PixelList;
         
   
   min_x = min(cell_pos(:,1));  
   min_y = min(cell_pos(:,2));
   max_x = max(cell_pos(:,1));
   max_y = max(cell_pos(:,2));

[X,Y]            = meshgrid(min_x:max_x,min_y:max_y);
ind_inside_cell  = double(inpolygon(X,Y,cell_pos(:,1), cell_pos(:,2)));

im_crop    = image_H2B(min_y:max_y,min_x:max_x);

   
centroid_x = mean(reg(i_cell).PixelList(:,1));
centroid_y = mean(reg(i_cell).PixelList(:,2));
   
    
    
feature_cell              = mitosis_feature_calculation_v1(im_crop,ind_inside_cell);
mitotic_phase             = predict(handles.Mdl,struct2array(feature_cell));

method = handles.method.String{handles.method.Value};

if strcmp(method,'Random Forest')

mitotic_phase = str2num(mitotic_phase{1});

end

reg(i_cell).mitotic_phase    = mitotic_phase;
feature_cell.mitotic_phase   = mitotic_phase;
feature_cell.cell_ID         = i_cell;
feature_cell.file_ID         = fullfile(handles.folder_image,handles.image_name);
feature_cell.centroid        = [centroid_x centroid_y];
handles.feature_cell(i_cell) = feature_cell;




end

handles.CC  = CC;
handles.reg = reg; 

cell_centroid = vertcat(handles.feature_cell.centroid);
cell_centroid = [cell_centroid(:,1) cell_centroid(:,2)];

handles.cell_centroid = cell_centroid; 


