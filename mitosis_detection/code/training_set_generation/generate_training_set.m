%% Script to create a training table 

%- The training table can come from the intitial manual cropping I have
%  done or also using the GUI allowing correction and saving of the table.


training_origin = readtable('training_set.csv'); %- Get the first one I generated



%- Load the files from the GUI

file_table = dir('*.csv')

table_tot = table(); 
for i_table = 1:length(file_table)
    
    table_t = readtable(file_table(i_table).name);
    table_tot = [ table_tot ; table_t];
    
end
    
table_feat_training = table_tot(:,1:123); %-remove keys variables (centroid, file_ID, cell_ID)
training_tot        = [training_origin ; table_feat_training]; %- merging the two training sets


class_distribution  =  tabulate(training_tot.mitotic_phase);
min_number_balanced =  min(class_distribution(:,2));  %- What is the minimum number of cell in category (aka the max number of cell in balanced data set)


ind_mitotic_1   = find(training_tot.mitotic_phase == 1)
a               = randperm(length(ind_mitotic_1))
ind_mitotic_1   = ind_mitotic_1(a(1:min_number_balanced))  %- Randomly picked this number of cell for every class

ind_mitotic_2   = find(training_tot.mitotic_phase == 2)
a               = randperm(length(ind_mitotic_2))
ind_mitotic_2   = ind_mitotic_2(a(1:min_number_balanced))

ind_mitotic_3   = find(training_tot.mitotic_phase == 3)
a               = randperm(length(ind_mitotic_3))
ind_mitotic_3   = ind_mitotic_3(a(1:min_number_balanced))

ind_mitotic_4   = find(training_tot.mitotic_phase == 4)
a               = randperm(length(ind_mitotic_4))
ind_mitotic_4   = ind_mitotic_4(a(1:min_number_balanced))



training_balance = training_tot([ind_mitotic_1 ; ind_mitotic_2 ; ind_mitotic_3 ; ind_mitotic_4],:); %- balanced feature set

writetable(training_balance, 'training_set_v4_balanced.csv') %- Save the balanced feature set





%- Random forest 
%- Quick code to test the performance with the balanced training set  

feature_RF       = training_balance(:,1:end-1);
var_response     = table2array(training_balance(:,end));
Mdl_new          = TreeBagger(500,table2array(feature_RF),var_response,'NumPredictorsToSample',9,'OOBPrediction','on','InBagFraction',0.3)

a                = mean(oobError(Mdl_new));
t                = templateSVM('Standardize',1);

Mdl              = fitcecoc(training_balance,'mitotic_phase','Learners',t,'FitPosterior',1);


isLoss  = resubLoss(Mdl)
CVMdl   = crossval(Mdl);
oosLoss = kfoldLoss(CVMdl)

label = predict(Mdl,training_balance)




