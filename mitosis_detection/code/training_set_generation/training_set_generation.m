%%%%%% Generation of the training set for mitosis detection %%%%%%%

%- This is the very first script I used. This one is to open an image, and
%  to manually annotate cells. You put the name of the category in line 29,
%  and then you can cick on a cell that belongs to this category and it
%  will crop an image around and save it as e.g anaphase_1.tif.



%% Open an image 

image_name = 'well_E03_field_9_c1_t0137_z0001.tiff';
image      =    imread(image_name);

path_training_set = 'training_set'

if ~isdir(path_training_set)
    mkdir('training_set')  %- Create a folder to put our training images
end


%% Annotate and generate crop images of cell 

figure
imshow(image,[0 600])
[x, y]        = ginput(1);  %- This function start a cursor that you can click on the figure and get the position as x,y

box_x = [x-49 ; x+50]
box_y = [y-49 ; y+50]

im_cell    = image(box_y(1):box_y(2),box_x(1):box_x(2)); %- crop the image around 

imwrite(im_cell, fullfile('training_set',strcat('dead_',num2str(i_cell),'.tif'))) . %- Save the image
i_cell = i_cell +1;  %- update the indice



%% Do manual segmentation and save it like a small mask

%- Once you are done with selected cells for different categories, you have
%  to manually segment them (at least I did it like that at the beginning).
%  This part of the script help you to do that.


training_cell_image = dir('*.tif');  %- Get the list of cropped images you created before


image_cell = imread(training_cell_image(i_cell).name); %- read it

figure
imshow(image_cell,[]);
h = imfreehand;  %- imfreehand is like ginput except you can draw the outline of the cells
a = createMask(h); %- Get a binary mask from your drawing

imwrite(a,strcat('mask_',training_cell_image(i_cell).name));  %- write the mask image

i_cell = i_cell + 1; 


