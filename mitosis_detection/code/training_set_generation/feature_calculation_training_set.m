%%%%% SCRIPT TO EXTRACT FEATURE FROM THE TRAINING SET %%%%

%- After I generated the training set by croping cells from images and
%saved the masks, I needed to compute features from them. This is the
%script that allows to load them and compute features and then save the set
%of features into .csv



image_cell     = dir('**/*.tif') %- Get the file in the folder containing subfolder for each class of cell cycle.
ind_mask       = squeeze(cell2array(cellfun(@(x) strfind(x,'mask'), {image_cell.name},'UniformOutput',false))); %- who is a mask ? 

image_cell_raw  = image_cell(~ind_mask);  %- Raw images
image_cell_mask = image_cell(logical(ind_mask)); %- mask images

feature_cell_table = table ; %- intiate the table for features 
feature_struct = [] ; 

for i_cell = 1:length(image_cell_raw)

  
    
    image_raw            = imread(fullfile(image_cell_raw(i_cell).folder,image_cell_raw(i_cell).name));
    image_mask           = imread(fullfile(image_cell_mask(i_cell).folder,image_cell_mask(i_cell).name));  
    feature_cell         = mitosis_feature_calculation_v1(image_raw,image_mask); %- compute the feature for the cell

    
    dum                  = strsplit(image_cell_raw(i_cell).folder,'/'); 
      
    
    if strcmp(dum{end},'anaphase')  %- What is the class of the images ? contained in its name, then code it 1,2,3,4
          
    feature_cell.mitotic_class  = 1;
    
    elseif strcmp(dum{end},'metaphase')
    
        feature_cell.mitotic_class  = 2;

    elseif    strcmp(dum{end},'telophase')
        
        feature_cell.mitotic_class  = 3; 
        
    elseif    strcmp(dum{end},'dead')
        
        feature_cell.mitotic_class  = 4;    
        
        
    end
        
    table_cell                  = struct2table(feature_cell); %- get the set of feature as a 1-line table
    
    
    feature_cell_table(i_cell,:)    = table_cell;
    
end

writetable(feature_cell_table,'training_set.csv');  %- Write the features set in a .csv table




%% First test of classification 

%- Here I tried to check the out-of-bag error in the RF classifier (check out of bag on google if you need) 


mitotic_phase = feature_cell_table.mitotic_class;  

ind_telo   = mitotic_phase==1;
ind_meta   = mitotic_phase==2;
ind_ana    = mitotic_phase==3;
ind_dead   = mitotic_phase==4;

var_response  = ind_telo + 2*ind_meta + 3*ind_ana +4*ind_dead;
feature_RF    = feature_cell_table(:,1:end-1);

%- Random forest 


Mdl = TreeBagger(500,table2array(feature_RF),var_response,'NumPredictorsToSample',9,'OOBPrediction','on','InBagFraction',0.3) %- training of RF
a   = oobError(Mdl); %- Get the OOb error

save('Mdl')

%% First test on applying on new images 

%- here I tested to make predictions on images 


image_file= dir('*.tiff') %- Get the images in a folder


for i_image=1:10


image_H2B = imread(image_file(i_image).name);

param.h_maxima          = 5; %- Set the parameters of the segmentation function
param.otsu_thresh_index = 1;
param.otsu_coeff        = 1;
param.size_threshold    = 200;
param.method            = 'local_threshold'
param.n_class           = 2; 
param.show              = 0;
param.threshold_local   = 3; 
param.min_size_colony   = 10; 
param.DICE_threshold    = 0.5;

seg = segmentation_nucleus_v2(image_H2B,param); %- Run the segmentation

%- Get the crop image for every object and predict the mitotic class


CC  = bwconncomp(seg);
reg = regionprops(CC,'PixelList','PixelIdxList') %- Get a structure containing List of Pixel for every cells

for i_cell = 1:CC.NumObjects %- loop over each cells
    
    mask_cell = zeros(size(image_H2B,1), size(image_H2B,2)); %- initiate mask cells
    mask_cell(reg(i_cell).PixelIdxList) = 1; %- set the pixels of the cell to one
    
      
feature_cell = mitosis_feature_calculation_v1(image_H2B,mask_cell); %- Get the feature calculation for the cell

mitotic_phase = predict(Mdl,struct2array(feature_cell)); %- Run the prediction using the model
 
reg(i_cell).mitotic_phase = str2num(mitotic_phase{1}); %- Add it to reg structure (region props of the segmentation)


end



%- plot the results 

figure
imshow(image_H2B,[])

for i_cell = 1:CC.NumObjects
    
    hold all
    
    mask_cell = zeros(size(image_H2B,1), size(image_H2B,2));
    mask_cell(reg(i_cell).PixelIdxList) = 1; 
    a           = mask2poly(mask_cell);  %- Get the outline of the cell
    
    if reg(i_cell).mitotic_phase == 1
        
        plot(a(:,1),a(:,2),'.','col','red') 
        
    elseif reg(i_cell).mitotic_phase == 2
        
        plot(a(:,1),a(:,2),'.','col','green') 
        
    elseif  reg(i_cell).mitotic_phase == 3

       plot(a(:,1),a(:,2),'.','col','yellow')
       
    elseif  reg(i_cell).mitotic_phase == 4

       plot(a(:,1),a(:,2),'.','col','blue') 
       
    end


end


saveas(gcf,'frame_fig.png') %- save the figure
close(gcf) %- close the figure

frame_fig = imread('frame_fig.png'); %- load it again so its now an image

imwrite(frame_fig, 'movie_mitosis_classif.tif', 'WriteMode', 'append',  'Compression','none'); %- write it back so it add a frame to the video movie_mitosis_classif.tif


end













