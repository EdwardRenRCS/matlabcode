function feature_cell = mitosis_feature_calculation_vRG(image_raw,image_mask)


    CC_mask    = bwconncomp(image_mask);
    im_prop    = regionprops(image_mask,'PixelList');
    
    a          = min(im_prop.PixelList);
    b          = max(im_prop.PixelList);

    min_x      = a(1); min_y = a(2); max_x = b(1); max_y = b(2);
    cell_crop  = image_raw(min_y : max_y , min_x : max_x);
    mask_crop  = image_mask(min_y : max_y , min_x : max_x);

%%%%- computation of features 
    
    
%% Shape features


ind_outline  = boundary(im_prop.PixelList(:,1), im_prop.PixelList(:,2));
cell_outline = im_prop.PixelList(ind_outline,:);

geom_colony   =  polygeom(cell_outline(:,1),cell_outline(:,2)); 

dist_mat_cell = distmat(cell_outline);



%% Haralick features
    
 
cell_crop_mask = cell_crop.*uint16(mask_crop);

min_image_cell = min(cell_crop_mask(cell_crop_mask ~=0));
max_image_cell = max(cell_crop_mask(cell_crop_mask ~=0));
cell_crop_mask_normalized = (double(cell_crop_mask) - double(min_image_cell))./(double(max_image_cell) - double(min_image_cell));
cell_crop_mask_normalized(cell_crop_mask_normalized<0) = 0;



%%%%---- Not gray level normalized features

%- Distance of 1 pixel

distance_Har = 1;

glcm_hor   = graycomatrix(cell_crop_mask,'Offset',[0 distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_ver   = graycomatrix(cell_crop_mask,'Offset',[-distance_Har 0],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_diag1 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har -distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_diag2 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);


Haralick(:,1) = haralickTextureFeatures(glcm_hor);
Haralick(:,2) = haralickTextureFeatures(glcm_ver);
Haralick(:,3) = haralickTextureFeatures(glcm_diag1);
Haralick(:,4) = haralickTextureFeatures(glcm_diag2);

Haralick_feat = mean(Haralick,2);


feature_cell.Ang_2nd_moment_dist1   =  Haralick_feat(1);
feature_cell.contrast_dist1         =  Haralick_feat(2);
feature_cell.Correlation_dist1      =  Haralick_feat(3);
feature_cell.Variance_dist1         =  Haralick_feat(4);
feature_cell.Inv_dif_moment_dist1   =  Haralick_feat(5);
feature_cell.sum_avg_dist1          =  Haralick_feat(6);
feature_cell.sum_var_entropy_dist1  =  Haralick_feat(7);
feature_cell.sum_entropy_dist1      =  Haralick_feat(8);
feature_cell.entropy_dist1          =  Haralick_feat(9);
feature_cell.diff_variance_dist1    =  Haralick_feat(10);
feature_cell.diff_entropy_dist1     =  Haralick_feat(11);
feature_cell.Info1_dist1            =  Haralick_feat(12);
feature_cell.Info2_dist1            =  Haralick_feat(13);
feature_cell.MCC_dist1              =  Haralick_feat(14);

%- Distance of 2 pixel

distance_Har = 2;
    

glcm_hor   = graycomatrix(cell_crop_mask,'Offset',[0 distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_ver   = graycomatrix(cell_crop_mask,'Offset',[-distance_Har 0],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_diag1 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har -distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_diag2 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);


Haralick(:,1) = haralickTextureFeatures(glcm_hor);
Haralick(:,2) = haralickTextureFeatures(glcm_ver);
Haralick(:,3) = haralickTextureFeatures(glcm_diag1);
Haralick(:,4) = haralickTextureFeatures(glcm_diag2);

Haralick_feat = mean(Haralick,2);


feature_cell.Ang_2nd_moment_dist2   =  Haralick_feat(1);
feature_cell.contrast_dist2         =  Haralick_feat(2);
feature_cell.Correlation_dist2      =  Haralick_feat(3);
feature_cell.Variance_dist2         =  Haralick_feat(4);
feature_cell.Inv_dif_moment_dist2   =  Haralick_feat(5);
feature_cell.sum_avg_dist2          =  Haralick_feat(6);
feature_cell.sum_var_entropy_dist2  =  Haralick_feat(7);
feature_cell.sum_entropy_dist2      =  Haralick_feat(8);
feature_cell.entropy_dist2          =  Haralick_feat(9);
feature_cell.diff_variance_dist2    =  Haralick_feat(10);
feature_cell.diff_entropy_dist2     =  Haralick_feat(11);
feature_cell.Info1_dist2            =  Haralick_feat(12);
feature_cell.Info2_dist2            =  Haralick_feat(13);
feature_cell.MCC_dist2              =  Haralick_feat(14);    
    


%- Distance of 4 pixel

    
distance_Har = 4;
    

glcm_hor   = graycomatrix(cell_crop_mask,'Offset',[0 distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_ver   = graycomatrix(cell_crop_mask,'Offset',[-distance_Har 0],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_diag1 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har -distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_diag2 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);


Haralick(:,1) = haralickTextureFeatures(glcm_hor);
Haralick(:,2) = haralickTextureFeatures(glcm_ver);
Haralick(:,3) = haralickTextureFeatures(glcm_diag1);
Haralick(:,4) = haralickTextureFeatures(glcm_diag2);

Haralick_feat = mean(Haralick,2);


feature_cell.Ang_2nd_moment_dist4   =  Haralick_feat(1);
feature_cell.contrast_dist4         =  Haralick_feat(2);
feature_cell.Correlation_dist4      =  Haralick_feat(3);
feature_cell.Variance_dist4         =  Haralick_feat(4);
feature_cell.Inv_dif_moment_dist4   =  Haralick_feat(5);
feature_cell.sum_avg_dist4          =  Haralick_feat(6);
feature_cell.sum_var_entropy_dist4  =  Haralick_feat(7);
feature_cell.sum_entropy_dist4      =  Haralick_feat(8);
feature_cell.entropy_dist4          =  Haralick_feat(9);
feature_cell.diff_variance_dist4    =  Haralick_feat(10);
feature_cell.diff_entropy_dist4     =  Haralick_feat(11);
feature_cell.Info1_dist4            =  Haralick_feat(12);
feature_cell.Info2_dist4            =  Haralick_feat(13);
feature_cell.MCC_dist4              =  Haralick_feat(14);    
    

%- Distance of 8 pixel
    

distance_Har = 8;
    

glcm_hor   = graycomatrix(cell_crop_mask,'Offset',[0 distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_ver   = graycomatrix(cell_crop_mask,'Offset',[-distance_Har 0],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_diag1 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har -distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);
glcm_diag2 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop)) max(max(cell_crop))]);

try
    
Haralick(:,1) = haralickTextureFeatures(glcm_hor);
Haralick(:,2) = haralickTextureFeatures(glcm_ver);
Haralick(:,3) = haralickTextureFeatures(glcm_diag1);
Haralick(:,4) = haralickTextureFeatures(glcm_diag2);

Haralick_feat = mean(Haralick,2);
catch
    Haralick_feat = nan(14,1)
end

feature_cell.Ang_2nd_moment_dist8   =  Haralick_feat(1);
feature_cell.contrast_dist8         =  Haralick_feat(2);
feature_cell.Correlation_dist8      =  Haralick_feat(3);
feature_cell.Variance_dist8         =  Haralick_feat(4);
feature_cell.Inv_dif_moment_dist8   =  Haralick_feat(5);
feature_cell.sum_avg_dist8          =  Haralick_feat(6);
feature_cell.sum_var_entropy_dist8  =  Haralick_feat(7);
feature_cell.sum_entropy_dist8      =  Haralick_feat(8);
feature_cell.entropy_dist8          =  Haralick_feat(9);
feature_cell.diff_variance_dist8    =  Haralick_feat(10);
feature_cell.diff_entropy_dist8     =  Haralick_feat(11);
feature_cell.Info1_dist8            =  Haralick_feat(12);
feature_cell.Info2_dist8            =  Haralick_feat(13);
feature_cell.MCC_dist8              =  Haralick_feat(14);      
    

%%%%----  Gray level normalized features


distance_Har = 1;

glcm_hor   = graycomatrix(cell_crop_mask_normalized,'Offset',[0 distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_ver   = graycomatrix(cell_crop_mask_normalized,'Offset',[-distance_Har 0],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_diag1 = graycomatrix(cell_crop_mask_normalized,'Offset',[-distance_Har -distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_diag2 = graycomatrix(cell_crop_mask_normalized,'Offset',[-distance_Har distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);


Haralick(:,1) = haralickTextureFeatures(glcm_hor);
Haralick(:,2) = haralickTextureFeatures(glcm_ver);
Haralick(:,3) = haralickTextureFeatures(glcm_diag1);
Haralick(:,4) = haralickTextureFeatures(glcm_diag2);

Haralick_feat = mean(Haralick,2);


feature_cell.Ang_2nd_moment_dist1_norm   =  Haralick_feat(1);
feature_cell.contrast_dist1_norm         =  Haralick_feat(2);
feature_cell.Correlation_dist1_norm      =  Haralick_feat(3);
feature_cell.Variance_dist1_norm         =  Haralick_feat(4);
feature_cell.Inv_dif_moment_dist1_norm   =  Haralick_feat(5);
feature_cell.sum_avg_dist1_norm         =  Haralick_feat(6);
feature_cell.sum_var_entropy_dist1_norm  =  Haralick_feat(7);
feature_cell.sum_entropy_dist1_norm      =  Haralick_feat(8);
feature_cell.entropy_dist1_norm          =  Haralick_feat(9);
feature_cell.diff_variance_dist1_norm    =  Haralick_feat(10);
feature_cell.diff_entropy_dist1_norm     =  Haralick_feat(11);
feature_cell.Info1_dist1_norm            =  Haralick_feat(12);
feature_cell.Info2_dist1_norm            =  Haralick_feat(13);
feature_cell.MCC_dist1_norm              =  Haralick_feat(14);

%- Distance of 2 pixel

distance_Har = 2;
    

glcm_hor   = graycomatrix(cell_crop_mask_normalized,'Offset',[0 distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_ver   = graycomatrix(cell_crop_mask_normalized,'Offset',[-distance_Har 0],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_diag1 = graycomatrix(cell_crop_mask_normalized,'Offset',[-distance_Har -distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_diag2 = graycomatrix(cell_crop_mask_normalized,'Offset',[-distance_Har distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);


Haralick(:,1) = haralickTextureFeatures(glcm_hor);
Haralick(:,2) = haralickTextureFeatures(glcm_ver);
Haralick(:,3) = haralickTextureFeatures(glcm_diag1);
Haralick(:,4) = haralickTextureFeatures(glcm_diag2);

Haralick_feat = mean(Haralick,2);


feature_cell.Ang_2nd_moment_dist2_norm   =  Haralick_feat(1);
feature_cell.contrast_dist2_norm         =  Haralick_feat(2);
feature_cell.Correlation_dist2_norm      =  Haralick_feat(3);
feature_cell.Variance_dist2_norm         =  Haralick_feat(4);
feature_cell.Inv_dif_moment_dist2_norm   =  Haralick_feat(5);
feature_cell.sum_avg_dist2_norm          =  Haralick_feat(6);
feature_cell.sum_var_entropy_dist2_norm  =  Haralick_feat(7);
feature_cell.sum_entropy_dist2_norm      =  Haralick_feat(8);
feature_cell.entropy_dist2_norm          =  Haralick_feat(9);
feature_cell.diff_variance_dist2_norm    =  Haralick_feat(10);
feature_cell.diff_entropy_dist2_norm     =  Haralick_feat(11);
feature_cell.Info1_dist2_norm            =  Haralick_feat(12);
feature_cell.Info2_dist2_norm            =  Haralick_feat(13);
feature_cell.MCC_dist2_norm              =  Haralick_feat(14);    
    


%- Distance of 4 pixel

    
distance_Har = 4;
    

glcm_hor   = graycomatrix(cell_crop_mask,'Offset',[0 distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_ver   = graycomatrix(cell_crop_mask,'Offset',[-distance_Har 0],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_diag1 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har -distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_diag2 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);


Haralick(:,1) = haralickTextureFeatures(glcm_hor);
Haralick(:,2) = haralickTextureFeatures(glcm_ver);
Haralick(:,3) = haralickTextureFeatures(glcm_diag1);
Haralick(:,4) = haralickTextureFeatures(glcm_diag2);

Haralick_feat = mean(Haralick,2);


feature_cell.Ang_2nd_moment_dist4_norm   =  Haralick_feat(1);
feature_cell.contrast_dist4_norm         =  Haralick_feat(2);
feature_cell.Correlation_dist4_norm      =  Haralick_feat(3);
feature_cell.Variance_dist4_norm         =  Haralick_feat(4);
feature_cell.Inv_dif_moment_dist4_norm  =  Haralick_feat(5);
feature_cell.sum_avg_dist4_norm          =  Haralick_feat(6);
feature_cell.sum_var_entropy_dist4_norm  =  Haralick_feat(7);
feature_cell.sum_entropy_dist4_norm      =  Haralick_feat(8);
feature_cell.entropy_dist4_norm          =  Haralick_feat(9);
feature_cell.diff_variance_dist4_norm    =  Haralick_feat(10);
feature_cell.diff_entropy_dist4_norm     =  Haralick_feat(11);
feature_cell.Info1_dist4_norm            =  Haralick_feat(12);
feature_cell.Info2_dist4_norm            =  Haralick_feat(13);
feature_cell.MCC_dist4_norm              =  Haralick_feat(14);    
    

%- Distance of 8 pixel
    

distance_Har = 8;
    

glcm_hor   = graycomatrix(cell_crop_mask,'Offset',[0 distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_ver   = graycomatrix(cell_crop_mask,'Offset',[-distance_Har 0],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_diag1 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har -distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);
glcm_diag2 = graycomatrix(cell_crop_mask,'Offset',[-distance_Har distance_Har],'NumLevels',32,'GrayLimits',[min(min(cell_crop_mask_normalized)) max(max(cell_crop_mask_normalized))]);


Haralick(:,1) = haralickTextureFeatures(glcm_hor);
Haralick(:,2) = haralickTextureFeatures(glcm_ver);
Haralick(:,3) = haralickTextureFeatures(glcm_diag1);
Haralick(:,4) = haralickTextureFeatures(glcm_diag2);

Haralick_feat = mean(Haralick,2);


feature_cell.Ang_2nd_moment_dist8_norm   =  Haralick_feat(1);
feature_cell.contrast_dist8_norm         =  Haralick_feat(2);
feature_cell.Correlation_dist8_norm      =  Haralick_feat(3);
feature_cell.Variance_dist8_norm         =  Haralick_feat(4);
feature_cell.Inv_dif_moment_dist8_norm   =  Haralick_feat(5);
feature_cell.sum_avg_dist8_norm          =  Haralick_feat(6);
feature_cell.sum_var_entropy_dist8_norm  =  Haralick_feat(7);
feature_cell.sum_entropy_dist8_norm      =  Haralick_feat(8);
feature_cell.entropy_dist8_norm          =  Haralick_feat(9);
feature_cell.diff_variance_dist8_norm    =  Haralick_feat(10);
feature_cell.diff_entropy_dist8_norm     =  Haralick_feat(11);
feature_cell.Info1_dist8_norm            =  Haralick_feat(12);
feature_cell.Info2_dist8_norm            =  Haralick_feat(13);
feature_cell.MCC_dist8_norm              =  Haralick_feat(14);      
    

    
%% Gray level features

%- Unormalized

feature_cell.mean_value_gray = mean(cell_crop_mask(cell_crop_mask ~=0));
feature_cell.std_value_gray  = std(double(cell_crop_mask(cell_crop_mask ~=0)));

%- Normalized

feature_cell.mean_value_gray_norm = mean(cell_crop_mask(cell_crop_mask ~=0));
feature_cell.std_value_gray_norm  = std(double(cell_crop_mask(cell_crop_mask ~=0)));

