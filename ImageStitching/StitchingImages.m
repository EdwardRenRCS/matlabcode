%%% SCRIPT TO CONCATENATE IMAGES 

%% Get images structure 
%Select folders containing images to be stitched
folder_well   = uipickfiles
%Define output older
folder_output = 'E:\leverjs\Ed\20201230SeongminData\ConcatenatedForAndy';

if ~exist(folder_output,'dir')
    mkdir(folder_output);
end

file_image    = [] ; 


for i_well = 1:length(folder_well)

cd(folder_well{i_well})

file_image_well  = dir(fullfile('*.tif'));
file_image_well = file_image_well(cellfun(@(x) strcmp('Assay',x(1:5)), {file_image_well.name}));

for i_image = 1:length(file_image_well)
    
    
    fname = file_image_well(i_image).name ;

%     day=regexp(folder_well{i_well},'-D(\d)','tokens','once');
%     day = str2double(day);
    day= i_well;
    
    channel=regexp(fname,'C(\d{2,}).tif','tokens','once');
    numchannel=str2double(channel);
    
    time=regexp(fname,'_T(\d{4,})','tokens','once');
    numtime=str2double(time);
    
    field=regexp(fname,'F(\d{3,})L','tokens','once');
    numfield=str2double(field);
    
    well=regexp(fname,'_(...)_T','tokens','once');

 day

file_image_well(i_image).channel = channel ;  
file_image_well(i_image).time    = time ;  
file_image_well(i_image).well    = well ;  
file_image_well(i_image).field   = field ;  
file_image_well(i_image).day     = day ;

file_image_well(i_image).channel = numchannel ;  
file_image_well(i_image).time    = numtime ;  
file_image_well(i_image).field   = numfield ;  

end
file_image = [file_image ; file_image_well ];

end


table_files = struct2table(file_image);
for day=2:max(table_files.day)
    table_files{(table_files.day==day),'time'}=table_files{(table_files.day==day),'time'}...
        +max(table_files{(table_files.day<day),'time'});
end

%% Concatenate and save images



a          = imread(fullfile(table_files.folder{1}, table_files.name{1}));
image_size = size(a);

%Define Wells,Fields and channels to be stitched together.
well = unique(table_files.well);
matrix_field = [1 2 3; 5 6 7; 9 10 11];
channel_field = [1,2,3];


for i_well = 1:length(well)
    
    well_ID = well{i_well};
    file_image_well = table2struct(table_files(contains(table_files.well,well_ID),:));
    
    time_frame = unique([file_image_well.time]);
    field_view = unique([file_image_well.field]);
    channel    = unique([file_image_well.channel]);
    
    for i = 1:length(time_frame)

        
        i
        file_image_well_time = file_image_well([file_image_well.time] == time_frame(i));
        
        
%         for i_channel = 1:length(channel)
        for iterator_channel = 1:length(channel_field)
            
            i_channel = channel_field(iterator_channel);
            
            
            file_image_well_time_channel = file_image_well_time([file_image_well_time.channel] == i_channel)    ;  
            
            if ~isempty(file_image_well_time_channel)
            
            big_image                    = zeros(size(matrix_field,1)*image_size(1), size(matrix_field,2)*image_size(2));
            
            
            for i_field = 1:numel(matrix_field)
                
                matrix_index = matrix_field';
                current_field = matrix_index(i_field);
                [ind_field_x ind_field_y] = ind2sub(size(matrix_field), find(matrix_field == current_field));
                
                file_image_well_time_channel_field = file_image_well_time_channel([file_image_well_time_channel.field] == current_field,:);
                if ~isempty(file_image_well_time_channel_field)
                    
                    image = imread(fullfile(file_image_well_time_channel_field(1).folder, file_image_well_time_channel_field(1).name));
                    
                    big_image((ind_field_x-1)*image_size(1) +1:(ind_field_x-1)*image_size(1) + image_size(1),(ind_field_y-1)*image_size(2) +1:(ind_field_y-1)*image_size(2) + image_size(2)) = image;
                end
            end
            
            
            fname = file_image_well_time_channel(1).name;
%             fname = file_image_well_time(1).name;

            [cStart]= regexp(fname,'C\d+.tif');
            [tStart]= regexp(fname,'_T\d+');
            
            fname(tStart+2:cStart-1) = [];
            fname = insertAfter(fname,'_T',strcat(num2str(file_image_well_time(1).time,'%04d'),'_'));

            imwrite(uint16(big_image), fullfile(folder_output,fname));
            
            end
            
            
        end
        
        
    end
    
    
end







