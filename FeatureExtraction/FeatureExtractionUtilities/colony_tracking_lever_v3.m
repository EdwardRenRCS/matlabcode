function [movie_struct mask_cell_colony] =  colony_tracking_lever_v1(movie_struct, CONSTANTS, param)


%- if a movie of the colony track needs to be exported, we initiate it
% if param.movie_colony
%     mask_cell_colony = zeros(CONSTANTS.imageData.Dimensions(2),CONSTANTS.imageData.Dimensions(1),CONSTANTS.imageData.NumberOfFrames);
% end


%- We create a grid, for convenience in the future to create mask cell
grid_x = repmat(transpose(1:1:CONSTANTS.imageData.Dimensions(2)),1,CONSTANTS.imageData.Dimensions(1));
grid_y = repmat(1:1:CONSTANTS.imageData.Dimensions(1),CONSTANTS.imageData.Dimensions(2),1);



for frame = 1:CONSTANTS.imageData.NumberOfFrames
    
    
    
    disp(strcat('processing frame n',num2str(frame),{' '},'out of',{' '}, num2str(CONSTANTS.imageData.NumberOfFrames)))
    
    frame_struct = movie_struct{1,frame}; %- Get the struct of the current frame
    if isempty(frame_struct)
        continue
    end
    n_cell       = size(frame_struct,2); %- How many cells in it
    
    centroid_cell_frame = zeros(n_cell,2);
    
    
    for i = 1:n_cell
        
        
        centroid_cell            = frame_struct(i).centroid;
        centroid_cell_frame(i,:) = centroid_cell(1:2); %- Get the centroid
        
        
        
%         if param.movie_colony
%             ind_cell =  find(inpolygon( grid_x,grid_y,frame_struct(i).surface(:,2),frame_struct(i).surface(:,1))==1);
%             frame_struct(i).PixelIdxList = ind_cell ; %- Get the cell mask if movie outputed
%         end
        
        
    end
    
    
    
    
    res_clust = dbscan(centroid_cell_frame,2,105) + 1 ; %- do dbscan to cluster the cells centroid
    res_clust(find(res_clust == 0)) = res_clust(find(res_clust == 0)) + 1;
    
    
    
    
    colony = unique(res_clust);  %- Get the value of dbscan ID
    colony(find(colony == 1)) = [] ; %- Remove the 1 as it is the ID of cells with no cluster
    
    
    if frame  == 2
        new_ID = length(colony) +2; %initiate the new_ID indice. needs to be superior of the highest colony ID currently.
    end
    
    
    for i = 1:n_cell
        
        movie_struct{1,frame}(i).colony_ID = res_clust(i); %- assign the dbscan results as colony ID
        
    end
    
    
    
    if frame > 2
        
        ind_colony_movie = {};
        
        
        
        time_t   = movie_struct{1,last_frame}; %- Get the previous frame
        time_t_1 = movie_struct{1,frame};  %- Get the current frame
        
        
        colony_t_1 = unique([time_t_1.colony_ID]);
        colony_t_1(find(colony_t_1 == 1)) = [] ;  %- colony ID at t-1
        
        
        colony_t = unique([time_t.colony_ID]);
        colony_t(find(colony_t == 1)) = [] ; %- colony ID at t
        
        colony_mat = zeros(length(colony_t_1), length(colony_t)); %- Initialize the matrix with proportion of shared cells
        
        
        for i_colony_t_1 = 1:length(colony_t_1)
            
            col_struct = time_t_1([time_t_1.colony_ID]==colony_t_1(i_colony_t_1));
            ID_cell    = [col_struct.ID];
            
            for i_colony_t = 1:length(colony_t)
                
                col_struct_temp = time_t([time_t.colony_ID]==colony_t(i_colony_t));
                ID_cell_t         = [col_struct_temp.ID];
                
                
                col_distri = double(ismember(ID_cell,ID_cell_t));
                
                colony_mat(i_colony_t_1,i_colony_t) = mean(col_distri); %- percentage of cells shared between the two colonies
                
            end
            
        end
        
        
        threshold  = 0.15;
        assoc_mat  = colony_mat>threshold; %- we threshold this to get the assignment matrix
        
        
        
        %- Assignment of colony and parents
        
        col_t_1_cell   = {};
        corr_ID_colony = [];
        
        
        for i_colony_t_1 = 1:size(assoc_mat,1)
            
            col_t_1_struct = [] ;
            col_t_1_struct(1).ID  = colony_t_1(i_colony_t_1);
            col_t_corr            = find(assoc_mat(i_colony_t_1,:) == 1);
            corr_ID_colony(i_colony_t_1,1) = colony_t_1(i_colony_t_1);
            
            
            if length(col_t_corr) == 1
                
                col_t_corr_bis =  find(assoc_mat(:,col_t_corr) == 1);
                
                if length(col_t_corr_bis) == 1
                    
                    col_t_1_struct(1).ID_corr      = colony_t(col_t_corr);
                    col_t_1_struct(1).parents      = colony_t(col_t_corr);
                    corr_ID_colony(i_colony_t_1,2) = colony_t(col_t_corr);
                    
                else
                    
                    col_t_1_struct(1).ID_corr      = new_ID;
                    col_t_1_struct(1).parents      = colony_t(col_t_corr);
                    corr_ID_colony(i_colony_t_1,2) = new_ID;
                    new_ID                         = new_ID + 1 ;
                    
                end
                
                
                
            else
                col_t_1_struct(1).ID_corr = new_ID;
                col_t_1_struct(1).parents = colony_t(col_t_corr);
                corr_ID_colony(i_colony_t_1,2) = new_ID;
                new_ID = new_ID + 1 ;
            end
            
            
            col_t_1_cell{i_colony_t_1} = col_t_1_struct ;
            
            
            
        end
        
        
        
        % - Modification of the t_1 struct
        
        for i_cell = 1:length(movie_struct{1,frame,1})
            
            if  movie_struct{1,frame,1}(i_cell).colony_ID ~= 1
                
                
                old_ID                                        = movie_struct{1,frame,1}(i_cell).colony_ID;
                
                movie_struct{1,frame,1}(i_cell).colony_ID     =  corr_ID_colony(find(corr_ID_colony(:,1) == old_ID),2);
                
                
                for i_col = 1:size(col_t_1_cell,2)
                    
                    ID(i_col)  = col_t_1_cell{1,i_col}.ID;
                    
                end
                
                
                ind_col = find(ID == old_ID);
                
                movie_struct{1,frame,1}(i_cell).colony_parent =  col_t_1_cell{1,ind_col}.parents;
                
                
                
            end
            
        end
        
        
    end
    
    
    
    if param.movie_colony  %- if movie export we create a movie with different label for cell from different colonies
        
        
        mask_colony_cell = zeros(CONSTANTS.imageData.Dimensions(2),CONSTANTS.imageData.Dimensions(1)); %- Initiate the image
        
        for i =1:n_cell
            
            mask_colony_cell(frame_struct(i).PixelIdxList) = movie_struct{1,frame,1}(i).colony_ID; %- Assign colony id to pixel value
            
        end
        mask_cell_colony(:,:,frame) = mask_colony_cell;
        
        
    else
        
        mask_cell_colony = [] ;
        
    end
    
    
    last_frame = frame;
end

