copyQuery = ['SELECT * FROM single_cell']

files = dir('*\single_cell_features*.db')
tableCat = table();
allPoints = table();
% chosenFiles = [1,3,4,5,6,7];
for i_files = 1:2
    currentFile = files(i_files);
    dbFile = fullfile(currentFile.folder,currentFile.name);
    
    conn = database(dbFile, '','', 'org.sqlite.JDBC', 'jdbc:sqlite:');
    dbTable = fetch(conn, copyQuery,'DataReturnFormat','table');
    
    dbTableFiltered = dbTable(~isnan(dbTable.green),:);
    dbTableFiltered = dbTableFiltered(~isnan(dbTableFiltered.voronoi_volume),:);

    currentLabel = i_files*ones(height(dbTableFiltered),1);
    dbTableFiltered.label = currentLabel;
    
    allPoints = cat(1,allPoints,dbTableFiltered);

end

allPointsStore = allPoints;
% allPoints.cell_ID = [];
% allPoints.frame = [];
allPoints.centroid_X = [];
allPoints.centroid_Y = [];
allPoints.col_ID = [];
allPoints.colony_centroid_angle = [];
allPoints.colony_border_angle = [];
allPoints.line_params = [];
allPoints.raw_angle = [];
allPoints.tan_params = [];
allPoints.X0 = [];
allPoints.Y0 = [];
allPoints.X0_in = [];
allPoints.Y0_in = [];


allPoints = allPoints(~isnan(allPoints.SRE),:);
extraStore = allPoints;

allPointsArray = table2array(allPoints);
truths = sum(isnan(allPointsArray));

allPoints = allPoints(:,~truths);

allPoints.apoptosis_probability = extraStore.apoptosis_probability;
allPoints.mitosis_probability = extraStore.mitosis_probability;

allPoints(isnan(allPoints.mitosis_probability),:) = [];
allPoints(isnan(allPoints.apoptosis_probability),:) = [];

shuffledTable2 = movevars(allPoints,'label','After','mitosis_probability');

shuffledTable2 = movevars(shuffledTable2,'cell_ID','Before','label');
shuffledTable2 = movevars(shuffledTable2,'frame','Before','label');

writetable(shuffledTable2,'FilteredFeatureTable.csv')