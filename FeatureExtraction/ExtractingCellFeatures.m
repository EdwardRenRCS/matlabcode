function ExtractingCellFeatures()


% feature_files = dir('*03*/colony_features.mat')
LEVER_STRUCT = struct()


param.movie_colony              = 0;

%- Get the path to the training set for the mitosis detection 
param.feature_training_path = 'FeatureExtractionUtilities'
param.feature_training_name = 'training_set_v6.csv' 

param.dPC_segmentation      = 0; 

LEVER_STRUCT = struct();

%- Provide web address for the LEVER file being read
LEVER_STRUCT.server     = 'https://leverjs.net/cx/'
LEVER_STRUCT.lever_file = strcat('g*Rafael*20210218StitchedImages*G03.LEVER')
%- Provide address of actual microscopy images
LEVER_STRUCT.lever_path = strcat('E:\leverjs\Ed\20201230SeongminData\ConcatenatedForAndy')
%- Provide write addres for SQ Lite database file storing the results
LEVER_STRUCT.write_path = strcat('\Output');
if ~exist(LEVER_STRUCT.write_path, 'dir')
    mkdir(LEVER_STRUCT.write_path)
end

%- Create the cell structure. It first read all the frames from a segmented
%- LEVER file and stores the results
%- into a structure so you have for every frame, the surface of the cells
%- etc...
field_name = 'G03';
[movie_struct CONSTANTS] = create_cell_struct_v3(LEVER_STRUCT.server ,LEVER_STRUCT.lever_file );


%- You also need to create the image struct. This will read the images in
%- folder and read their name to get info (channel, frame, Z, field, well)
%- and return a structure with all of that for every image. It is
%- convenient to have such a structure. 

% file_image = create_image_struct_v1(LEVER_STRUCT.lever_path)
file_image = create_image_struct_vConcatenatedv2(LEVER_STRUCT.lever_path)
file_image = file_image(strcmp({file_image.well},field_name))

%% Do the tracking of colony

%- Here you feed the function with the movie_struct you created and it
%- will do colony tracking. It will update the movie_struct by adding a
%- colony ID for every cell at every frame. 

%- hack to fill in empty frames from import, import process buggy, will fix
%later
% movie_struct = movie_struct(~cellfun('isempty',movie_struct));
% final_frame = movie_struct{end};
CONSTANTS.imageData.NumberOfFrames = length(movie_struct);

[ movie_struct mask_colony]     = colony_tracking_lever_v3(movie_struct, CONSTANTS, param);

if ~exist(LEVER_STRUCT.write_path,'dir')
    mkdir(LEVER_STRUCT.write_path);
end

table_raw_colony = table(); %initiate the colony features table

%- Create the struct of colony from the movie struct

[ colony colony_struct] = create_colony_struct_v2(movie_struct, CONSTANTS); %- this output a struct with the cells surface/centroid etc for every frame
%- and for every colonies
empty_haralick = load('FeatureExtractionUtilities/empty_haralick.mat')
%% Computing of the features


%-- Load model for mitosis classification

% [feature_training_name feature_training_path ] = uigetfile('*.csv')
feature_training      = readtable(fullfile(param.feature_training_path,param.feature_training_name));
t           = templateSVM('Standardize',1);
Mdl         = fitcecoc(feature_training,'mitotic_phase','Learners',t,'FitPosterior',1); %- fit the model

%%%%%%%%%- Loop over the frame, for each frame we will loop over the
%%%%%%%%- colonies present in the frame

feature_colony = [] ;
% 
%%
% % firstTimepoint = false;
% p = gcp('nocreate'); % If no pool, do not create new one.
% if isempty(p)
%     parpool(7);
% end
% for i_frame = 1:CONSTANTS.imageData.NumberOfFrames
table_raw_colony_store = [];
% for i_frame = 3
present_frames = unique([file_image.frame]);
frame_store2 = [];
%%
for i_frame = 1:length(present_frames)
    
    i_frame_image = present_frames(i_frame)
    c = clock;
%     disp(strcat('processing frame n',num2str(i_frame),{' '},'out of',{' '},num2str(CONSTANTS.imageData.NumberOfFrames),{' '},num2str(c(4)),':',num2str(c(5)))) ;
    
    
    table_raw_colony_1 = table_raw_colony_store;
    feature_frame = [];
    
    ind_colony         = find(cellfun(@(x) ~isempty(x), {colony_struct{:,i_frame}})); %- find colony present in the frame
    
    
    fucci_green_channel = 3; %- Set the channel identifier
    fucci_red_channel   = 2;
    H2B_channel         = 1;
    
    
    
    file_frame          = file_image([file_image.frame] == i_frame_image);
%     file_frame = file_image([file_image.frame] == movie_struct{i_frame}(1).time);
%     file_frame = file_image([file_image.frame] == movie_struct(i_frame,:).time);


    file_fucci_green    = file_frame([file_frame.channel] == fucci_green_channel); %- Get the file name
    file_fucci_red      = file_frame([file_frame.channel] == fucci_red_channel);
    file_H2B            = file_frame([file_frame.channel] == H2B_channel);
    file_fucci_red.name;
    frame_store2 = cat(2,frame_store2,file_fucci_red.frame);

    if ~isempty(file_fucci_green)
        
        
        image_fucci_green   = imread(fullfile(file_fucci_green.folder, file_fucci_green.name)); %- Get the image
        image_fucci_red     = imread(fullfile(file_fucci_red.folder, file_fucci_red.name));
        image_H2B           = imread(fullfile(file_H2B.folder, file_H2B.name));
        
        im_filtered_H2B   = averagefilter(image_H2B, [50 50], 'symmetric'); % mean filter to estimate bgd
        im_filtered_green   = averagefilter(image_fucci_green, [50 50], 'symmetric'); % mean filter to estimate bgd
        im_filtered_red     = averagefilter(image_fucci_red, [50 50], 'symmetric'); % mean filter to estimate bgd
        
        im_bgd_reduced_H2B                    = image_H2B - im_filtered_H2B; %remove background from image
        im_bgd_reduced_H2B(im_bgd_reduced_H2B < 0) = 0; %set the negative pixels to 0

        
        im_bgd_reduced_green                    = image_fucci_green - im_filtered_green; %remove background from image
        im_bgd_reduced_green(im_bgd_reduced_green < 0) = 0; %set the negative pixels to 0
        
        im_bgd_reduced_red                         = image_fucci_red - im_filtered_red; %remove background from image
        im_bgd_reduced_red(im_bgd_reduced_red < 0) = 0; %set the negative pixels to 0
        
        h2b_gradient = imgradient(image_H2B);
        green_gradient = imgradient(image_fucci_green);
        red_gradient = imgradient(image_fucci_red);
        file_fucci_green = [];
        table_raw_colony_store = [];

    elseif ~isempty(file_H2B)
        continue
    else
        continue
        disp(strcat('No Image Found.', num2str(i_frame))) ;

    end
    


frame_struct              =  movie_struct{1,i_frame};
%% Process the colony (feature etc...)
table_raw_cell = cell(length(ind_colony),1);
for pre_colony = 1:length(ind_colony)
    struct_colony = colony_struct{ind_colony(pre_colony),i_frame}; %- Get the colony
end

current_colony_struct = colony_struct(ind_colony,i_frame);
% Iterate through each colony calculating features for all the cells inside
% the colony
for i_colony = 1:length(ind_colony)
    i_colony/length(ind_colony)

%Stop colony_struct from being a broadcast variable.
    temp_colony = current_colony_struct;
    struct_colony = temp_colony{i_colony};
    
    feature_frame = struct('number_cell_colony',0,'average_cell_size',0,'colony_size',0,'colony_polygon',0,'density',0,'pos_colony',[0 0],'ratio_green',0,'distance_green_centroid',0,'distance_red_centroid',0,'roundess',0,'col_ID',0,'frame',0,'image',0);

    feature_frame.number_cell_colony = length(struct_colony);   %- number of cell in the colony
    
    cell_size          = cellfun(@(x) polyarea(x(:,1), x(:,2)) , {struct_colony.surface}) ;
    feature_frame.average_cell_size  = mean(cell_size); %- average cell size in the colony
    
    all_position       = vertcat(struct_colony.surface) ;
    
    colony_boundary    = boundary(all_position);
    feature_frame.colony_size        = polyarea(all_position(colony_boundary,1),all_position(colony_boundary,2)); %- size of the colony
    
    feature_frame.colony_polygon = [all_position(colony_boundary,1) all_position(colony_boundary,2)]; %- position of the colony
    
    colony_mask = poly2mask(all_position(colony_boundary,1),all_position(colony_boundary,2),size(image_H2B,1),size(image_H2B,2));
    boundary_pixels = bwboundaries(colony_mask);
    
    feature_frame.density            = feature_frame.number_cell_colony /feature_frame.colony_size; %- density of the colony
    
    
    
    s = regionprops(colony_mask,'Centroid','MajorAxisLength','MinorAxisLength','Orientation','Circularity','Eccentricity');
    s = s(1);
    short_angle = (s.Orientation)*pi/180;
    long_length = s.MajorAxisLength;
    centroid_coords = s.Centroid;
    colony_roundness = s.Circularity;
    colony_eccentricity = s.Eccentricity;
    xend = floor(centroid_coords(1)+long_length*sin(short_angle));
    yend = floor(centroid_coords(2)+long_length*cos(short_angle));
    
    xstart = floor(centroid_coords(1)-long_length*sin(short_angle));
    ystart = floor(centroid_coords(2)-long_length*cos(short_angle));
    [x_line,y_line] = bresenham(xstart,ystart,xend,yend);
    
    
    [Vin Von] = inpoly([x_line,y_line],feature_frame.colony_polygon);
    
    x_in = x_line(Vin);
    y_in = y_line(Vin);
    x_mid = x_in(ceil(end/2));
    y_mid = y_in(ceil(end/2));
    
    feature_frame.pos_colony = cat(2,x_mid,y_mid);
        
    centroid_cell = zeros(size(struct_colony,2),2) ;
    
    h2b_signal = NaN(size(struct_colony,2),1) ;
    green_signal = NaN(size(struct_colony,2),1) ;
    red_signal   = NaN(size(struct_colony,2),1) ;
    median_h2b_intensity = NaN(size(struct_colony,2),1) ;
    median_green_intensity = NaN(size(struct_colony,2),1) ;
    median_red_intensity = NaN(size(struct_colony,2),1) ;    
    integrated_h2b_intensity = NaN(size(struct_colony,2),1) ;
    integrated_green_intensity = NaN(size(struct_colony,2),1) ;
    integrated_red_intensity = NaN(size(struct_colony,2),1) ;
    stdev_h2b_intensity = NaN(size(struct_colony,2),1) ;
    stdev_green_intensity = NaN(size(struct_colony,2),1) ;
    stdev_red_intensity = NaN(size(struct_colony,2),1) ;
    mean_green_over_red = NaN(size(struct_colony,2),1) ;
    mean_h2b_gradient =  NaN(size(struct_colony,2),1) ; 
    mean_green_gradient =  NaN(size(struct_colony,2),1) ;
    mean_red_gradient =  NaN(size(struct_colony,2),1) ;
    median_h2b_gradient =  NaN(size(struct_colony,2),1) ;
    median_green_gradient =  NaN(size(struct_colony,2),1) ;
    median_red_gradient =  NaN(size(struct_colony,2),1) ;
    stdev_h2b_gradient =  NaN(size(struct_colony,2),1) ;
    stdev_green_gradient =  NaN(size(struct_colony,2),1) ;
    stdev_red_gradient =  NaN(size(struct_colony,2),1) ;
    kurtosis_h2b_gradient =  NaN(size(struct_colony,2),1) ;
    kurtosis_green_gradient = NaN(size(struct_colony,2),1) ;
    kurtosis_red_gradient =  NaN(size(struct_colony,2),1) ;

    roundness = zeros(size(struct_colony,2),1) ;
    area_cell = zeros(size(struct_colony,2),1) ;
    colony_centroid_difference = zeros(size(struct_colony,2),1) ;
    colony_border_difference = zeros(size(struct_colony,2),1) ;
    distance_centroid_colony = zeros(size(struct_colony,2),1) ;
    distance_border_colony  = zeros(size(struct_colony,2),1) ;
    line_coords = cell(size(struct_colony,2),1) ;
    tan_coords = cell(size(struct_colony,2),1) ;
    cent_coords = cell(size(struct_colony,2),1) ;
    colony_centroid_pixels = cell(size(struct_colony,2),1) ;
    tan_params = zeros(size(struct_colony,2),1) ;
    tan_params_inverse = zeros(size(struct_colony,2),1) ;
    line_params = zeros(size(struct_colony,2),1) ;
    line_params_inverse = zeros(size(struct_colony,2),1) ;
    relative_centroid_angle = zeros(size(struct_colony,2),1) ;
    relative_border_angle = zeros(size(struct_colony,2),1) ;
    angle_cell = zeros(size(struct_colony,2),1) ;
    skel_length = zeros(size(struct_colony,2),1) ;
    skel_curve = NaN(size(struct_colony,2),1) ;
    mitotic_phase_store = zeros(size(struct_colony,2),1);
    metaphase_probability = zeros(size(struct_colony,2),1);
    debris_probability = zeros(size(struct_colony,2),1);
    mitosis_angle = zeros(size(struct_colony,2),1);
    border_mitosis_angle = zeros(size(struct_colony,2),1);
    a_ellipse = zeros(size(struct_colony,2),1);
    b_ellipse = zeros(size(struct_colony,2),1);
    X0 = zeros(size(struct_colony,2),1);
    Y0 = zeros(size(struct_colony,2),1);
    X0_in = zeros(size(struct_colony,2),1);
    Y0_in = zeros(size(struct_colony,2),1);
    long_axis = zeros(size(struct_colony,2),1);
    short_axis = zeros(size(struct_colony,2),1);
    
    eccentricity = zeros(size(struct_colony,2),1);
    solidity = zeros(size(struct_colony,2),1);
    perimeter = zeros(size(struct_colony,2),1);
    major_axis_length = zeros(size(struct_colony,2),1);
    minor_axis_length = zeros(size(struct_colony,2),1);
    max_feret_length = zeros(size(struct_colony,2),1);
    min_feret_length = zeros(size(struct_colony,2),1);
    equiv_diameter = zeros(size(struct_colony,2),1);
    compactness = zeros(size(struct_colony,2),1);
    aspect_ratio = zeros(size(struct_colony,2),1);
    shape_factor_1 = zeros(size(struct_colony,2),1);
    shape_factor_2 = zeros(size(struct_colony,2),1);
    
    emptyHaralickTable = struct2table(empty_haralick.empty_haralick.empty_haralick);
    haralickVars = emptyHaralickTable.Properties.VariableNames;
    haralick_features = array2table(NaN(size(struct_colony,2),122),'VariableNames',haralickVars);
    
    haralicVarsCut = haralickVars(7:end);
    redHaralickVars = strcat('red_',haralicVarsCut);
    greenHaralickVars = strcat('green_',haralicVarsCut);

    red_haralick_features =  array2table(NaN(size(struct_colony,2),116),'VariableNames',redHaralickVars);
    green_haralick_features =  array2table(NaN(size(struct_colony,2),116),'VariableNames',greenHaralickVars);
    
    zernikeNames = string();
    for zz = 1:28
        zernikeName = strcat('zernike_moment',num2str(zz));
        zernikeNames(zz) = zernikeName;
    end
    zernikeNameCell = cellstr(zernikeNames);
    zernike_features = array2table(NaN(size(struct_colony,2),28),'VariableNames',zernikeNameCell);

    invariantNames = string();
    for ff = 1:11
        invariantName = strcat('invariant_moment',num2str(ff));
        invariantNames(ff) = invariantName;
    end
    
    red_invariant_names = strcat('red_',invariantNames);
    green_invariant_names = strcat('green_',invariantNames);

    
    invariant_moments = array2table(NaN(size(struct_colony,2),11),'VariableNames',invariantNames);
    red_invariant_moments = array2table(NaN(size(struct_colony,2),11),'VariableNames',red_invariant_names);
    green_invariant_moments = array2table(NaN(size(struct_colony,2),11),'VariableNames',green_invariant_names);

    run_length_names = {'SRE','LRE','GLN','RLN','RP','LGRE','HGRE','SRLGE','SRHGE','LRLGE','LRHGE','GLV','RLV'};
    red_run_length_names = strcat('red_',run_length_names);
    green_run_length_names = strcat('green_',run_length_names);
    
    run_length_matrix = array2table(NaN(size(struct_colony,2),13),'VariableNames',run_length_names);
    red_run_length_matrix = array2table(NaN(size(struct_colony,2),13),'VariableNames',red_run_length_names);
    green_run_length_matrix = array2table(NaN(size(struct_colony,2),13),'VariableNames',green_run_length_names);

    
    size_zone_names = {'SZE','LZE','SZGLN','ZSN','ZP','LGZE','HGZE','SZLGE','SZHGE','LZLGE','LZHGE','SZGLV','ZSV'};   
    red_size_zone_names = strcat('red_',size_zone_names);
    green_size_zone_names = strcat('green_',size_zone_names);
    
    size_zone_matrix  = array2table(NaN(size(struct_colony,2),13),'VariableNames',size_zone_names);
    red_size_zone_matrix  = array2table(NaN(size(struct_colony,2),13),'VariableNames',red_size_zone_names);
    green_size_zone_matrix  = array2table(NaN(size(struct_colony,2),13),'VariableNames',green_size_zone_names);

    
    gray_tone_names = {'coarseness','contrast','busyness','complexity','strength'};
    red_gray_tone_names = strcat('red_',gray_tone_names);
    green_gray_tone_names = strcat('green_',gray_tone_names);
    
    gray_tone_difference  = array2table(NaN(size(struct_colony,2),5),'VariableNames',gray_tone_names);
    red_gray_tone_difference  = array2table(NaN(size(struct_colony,2),5),'VariableNames',red_gray_tone_names);
    green_gray_tone_difference  = array2table(NaN(size(struct_colony,2),5),'VariableNames',green_gray_tone_names);

    
%     parfor i_cell = 1:size(struct_colony,2)
    for i_cell = 1:size(struct_colony,2)

        
        cell_pos = struct_colony(i_cell).surface;
        
        if isempty(cell_pos)
            continue
        end
        
        min_x = min(cell_pos(:,1));
        min_y = min(cell_pos(:,2));
        max_x = max(cell_pos(:,1));
        max_y = max(cell_pos(:,2));
        
        [X,Y]            = meshgrid(min_x:max_x,min_y:max_y);
        
        xq = X(:);
        yq = Y(:);
        
        ind_inside_cell_temp = inpoly2(horzcat(xq,yq),cell_pos);
   
        ind_inside_cell = reshape(ind_inside_cell_temp,[size(X,1),size(X,2)]);
        
        x_cell = X(ind_inside_cell==1);
        y_cell = Y(ind_inside_cell==1);
        
        
        ind =  sub2ind([size(image_H2B,1) size(image_H2B,2)],y_cell,x_cell);
        
        h2b_signal(i_cell) = mean(im_bgd_reduced_H2B(ind));
        green_signal(i_cell)      = mean(im_bgd_reduced_green(ind)); %- Get green signal for every cell
        red_signal(i_cell)        = mean(im_bgd_reduced_red(ind));   %- Get red signal for every cell
        centroid_cell(i_cell,:)   = [mean(x_cell) mean(y_cell)];
        
        median_h2b_intensity(i_cell)  = median(im_bgd_reduced_H2B(ind)); 
        median_green_intensity(i_cell)  = median(im_bgd_reduced_green(ind)); 
        median_red_intensity(i_cell)  = median(im_bgd_reduced_red(ind)); 
        
        integrated_h2b_intensity(i_cell)  = sum(im_bgd_reduced_H2B(ind));
        integrated_green_intensity(i_cell)  = sum(im_bgd_reduced_green(ind)); 
        integrated_red_intensity(i_cell)  = sum(im_bgd_reduced_red(ind)); 
        
        stdev_h2b_intensity(i_cell)  = std(single(im_bgd_reduced_H2B(ind))); 
        stdev_green_intensity(i_cell)   = std(single(im_bgd_reduced_green(ind))); 
        stdev_red_intensity(i_cell)   = std(single(im_bgd_reduced_red(ind)));

        mean_green_over_red(i_cell)   = mean(im_bgd_reduced_green(ind))/mean(im_bgd_reduced_red(ind)); 
        
        mean_h2b_gradient(i_cell)  = mean(h2b_gradient(ind));
        mean_green_gradient(i_cell)  =  mean(green_gradient(ind)); 
        mean_red_gradient(i_cell)  =  mean(red_gradient(ind)); 
        
        median_h2b_gradient(i_cell)  = median(h2b_gradient(ind));
        median_green_gradient(i_cell)  =  median(green_gradient(ind)); 
        median_red_gradient(i_cell)  =  median(red_gradient(ind)); 
                
        stdev_h2b_gradient(i_cell)  = std(h2b_gradient(ind));
        stdev_green_gradient(i_cell)  =  std(green_gradient(ind)); 
        stdev_red_gradient(i_cell)  =  std(red_gradient(ind)); 
        
        kurtosis_h2b_gradient(i_cell)  = kurtosis(h2b_gradient(ind));
        kurtosis_green_gradient(i_cell)  =  kurtosis(green_gradient(ind)); 
        kurtosis_red_gradient(i_cell)  =  kurtosis(red_gradient(ind)); 
        
        current_centroid = [mean(x_cell) mean(y_cell)];

                
        %Generate skeleton of segmentation result
        mask_frame_logical = gpuArray(false(size(image_H2B,1),size(image_H2B,2)));
        
        mask_frame_logical(ind) = 1;
        
        se = strel('disk',10);
        
        mask_frame_opened = imopen(mask_frame_logical,se);
        if isempty(find(mask_frame_opened))
            se = strel('disk',3);
            mask_frame_opened = imopen(mask_frame_logical,se);
            if isempty(find(mask_frame_opened))
                mask_frame_opened = mask_frame_logical;
            end
        end
                
        individual_skeleton = bwskel(gather(mask_frame_opened,'MinBranchLength',15));
        if isempty(find(individual_skeleton))
            individual_skeleton = bwskel(logical(mask_frame_opened));
        end
        [is_row,is_col] = find(individual_skeleton);
        
        skel_distances = pdist([is_row,is_col]);
        if ~isempty(skel_distances)
            skel_length(i_cell) = max(skel_distances);
        end
        
        skel_curve(i_cell) = max(LineCurvature2D([is_row,is_col])');
        warning('off','all');
        
        skeleton_coords = cat(2,is_row,is_col);
        cell_centroid_pos = current_centroid([2 1]);
        [min_skel_dist, min_skel_ind] = mink(distmat(cell_centroid_pos,skeleton_coords),15);
        is_row = is_row(min_skel_ind);
        is_col = is_col(min_skel_ind);
        individual_line = polyfit(is_row,is_col,1);
        orig_skel_y = min(is_row):max(is_row);
        skel_y = floor(cell_centroid_pos(1))-15:floor(cell_centroid_pos(1))+15;
        skel_x = floor(polyval(individual_line,skel_y));
        
        individual_line2 = polyfit(is_col,is_row,1);
        orig_skel_x2 = min(is_col):max(is_col);
        skel_x2 = floor(cell_centroid_pos(2))-15:floor(cell_centroid_pos(2))+15;
        skel_y2 = floor(polyval(individual_line2,skel_x2));
        
        line_x_coords = cat(2,skel_x,skel_x2);
        line_y_coords = cat(2,skel_y,skel_y2);
        coords = cat(1,line_x_coords,line_y_coords);
        
        
        if length(orig_skel_y) <= length(orig_skel_x2)
            [x_coords,pindices] = sort(skel_x2);
            y_coords = skel_y2(pindices);
        else
            [y_coords,pindices] = sort(skel_y);
            x_coords = skel_x(pindices);
        end
        
        
        [y_line, x_line] = bresenham(x_coords(1),y_coords(1),x_coords(end),y_coords(end));
        
        line_coords{i_cell} = cat(2,x_line,y_line);
        line_params(i_cell) = individual_line(1);
        line_params_inverse(i_cell) = individual_line2(1);
        angle_cell(i_cell) = atan((x_line(end)-x_line(1))/(y_line(end)-y_line(1)))* 180/pi;
        
        geom                = polygeom(cell_pos(:,1), cell_pos(:,2));
        area_cell(i_cell)   = geom(1);
        roundness(i_cell,1) = 4*pi*geom(1)/(geom(4)^2);
        
        colony_centroid = fliplr(floor(feature_frame.pos_colony));
        colony_centroid_difference(i_cell) = atan((cell_centroid_pos(1) - colony_centroid(1))/(cell_centroid_pos(2) - colony_centroid(2))) * 180/pi;
        
        centroid_line = cat(1,cell_centroid_pos,floor(colony_centroid));
        centroid_formula = polyfit(centroid_line(:,2),centroid_line(:,1),1);
        
        if (cell_centroid_pos(2) < colony_centroid(2))
            centroid_x1 = floor(cell_centroid_pos(2)):colony_centroid(2);
        else
            centroid_x1 = floor(colony_centroid(2):cell_centroid_pos(2));
        end
        centroid_y1 = floor(polyval(centroid_formula,centroid_x1));
        
        
        [cx_line, cy_line] = bresenham(centroid_x1(1),centroid_y1(1),centroid_x1(end),centroid_y1(end));
        
        cent_coords{i_cell} = cat(2,cy_line,cx_line);
        
        colony_centroid_pixels{i_cell} = bresenham(current_centroid(1),current_centroid(2),feature_frame.pos_colony(1),feature_frame.pos_colony(2));
        distance_centroid_colony(i_cell)   = distmat(centroid_cell(i_cell,:),feature_frame.pos_colony) ;
        boundary_pixels_array = boundary_pixels{1};
        boundary_pixels_array = fliplr(boundary_pixels_array);
        [distance_border_colony(i_cell), ~]     = min(distmat(centroid_cell(i_cell,:),boundary_pixels_array));
        
        [~, nearest_indices] = mink(distmat(centroid_cell(i_cell,:),boundary_pixels_array),30);
        
        for ni = 1:length(nearest_indices)
            if(nearest_indices(ni) <=0)
                nearest_indices(ni) = length(boundary_pixels_array) + nearest_indices(ni);
            end
            
            if(nearest_indices(ni) > length(boundary_pixels_array))
                nearest_indices(ni) = nearest_indices(ni) - length(boundary_pixels_array);
            end
        end
        
        
        nearest_points = boundary_pixels_array(nearest_indices,:);
        
        border_tangent_formula = polyfit(nearest_points(:,2),nearest_points(:,1),1);
        tangent_x1 = min(nearest_points(:,2)):max(nearest_points(:,2));
        tangent_y1 = floor(polyval(border_tangent_formula,tangent_x1));
        
        [tany_line, tanx_line] = bresenham(tangent_x1(1),tangent_y1(1),tangent_x1(end),tangent_y1(end));
        
        
        tan_coords{i_cell} = cat(2,tany_line,tanx_line);
        
        tan_params(i_cell) = border_tangent_formula(1);
        
        colony_border_difference(i_cell) = atan((tany_line(end)-tany_line(1))/(tanx_line(end)-tanx_line(1)))* 180/pi;
        
        relative_centroid_angle(i_cell) = abs(colony_centroid_difference(i_cell) - angle_cell(i_cell));
        relative_border_angle(i_cell) = abs(colony_border_difference(i_cell) - angle_cell(i_cell));
        
        if relative_centroid_angle(i_cell) > 90
            relative_centroid_angle(i_cell) = 180 - relative_centroid_angle(i_cell);
        end
        
        if relative_border_angle(i_cell) > 90
            relative_border_angle(i_cell) = 180 - relative_border_angle(i_cell);
        end
        
        current_image = image_H2B;
        im_crop = current_image(min_y:max_y,min_x:max_x); %- image croped

        im_red_crop = image_fucci_red(min_y:max_y,min_x:max_x);
        im_green_crop = image_fucci_green(min_y:max_y,min_x:max_x);
        
        try
            feature_cell              = mitosis_feature_calculation_v1(im_crop,ind_inside_cell); %- get feature calculation
        catch
            feature_cell = [];
        end
        
        if ~isnan(im_red_crop)
            
            red_feature_cell          = mitosis_feature_calculation_vRG2(im_red_crop,ind_inside_cell);
            green_feature_cell        = mitosis_feature_calculation_vRG2(im_green_crop,ind_inside_cell);
            
            fields = fieldnames(red_feature_cell);
            red_fields = strcat('red_',fields);
            green_fields = strcat('green_',fields);
            
            red_feature_cell = cell2struct(struct2cell(red_feature_cell),red_fields);
            green_feature_cell = cell2struct(struct2cell(green_feature_cell),green_fields);

        else
            fields = emptyHaralickTable.Properties.VariableNames;
            fields_cut = fields(7:end);            
            NaN_temp = NaN(length(fields_cut),1);
            cell_temp = num2cell(NaN_temp);

            red_fields = strcat('red_',fields_cut);
            green_fields = strcat('green_',fields_cut);
            
            red_feature_cell = cell2struct(cell_temp,red_fields);
            green_feature_cell = cell2struct(cell_temp,green_fields);
        end
        
        Nlist = [0  1  1  2  2  2  3  3  3  3  4  4 4 4 4  5  5  5 5 5 5  6  6  6 6 6 6 6];
        Mlist = [0 -1  1 -2  0  2 -3 -1  1  3 -4 -2 0 2 4 -5 -3 -1 1 3 5 -6 -4 -2 0 2 4 6];
        
        zernikeArray = [];
        
        mask_crop = zeros(160,160);
        try
            mask_crop(80-size(ind_inside_cell,1)/2:79+size(ind_inside_cell,1)/2,80-size(ind_inside_cell,2)/2:79+size(ind_inside_cell,2)/2) = ind_inside_cell;
        catch
            continue
        end
        
        for zz = 1:length(Nlist)
            n = Nlist(zz);
            m = Mlist(zz);
            [~, zernikeAmplitude, ~] = Zernikmoment(mask_crop,n,m);      % Call Zernikemoment fuction
            zernikeArray = cat(2,zernikeArray,zernikeAmplitude);
        end
        
       
        zernike_features(i_cell,:) = array2table(zernikeArray,'VariableNames',zernikeNameCell);
        
        shape_features = regionprops(mask_crop,'Area','Eccentricity','Solidity','Perimeter','MajorAxisLength','MinorAxisLength','MaxFeretProperties','MinFeretProperties','EquivDiameter');
        eccentricity(i_cell) = shape_features(1).Eccentricity;
        solidity(i_cell) = shape_features(1).Solidity;
        major_axis_length(i_cell) = shape_features(1).MajorAxisLength;
        minor_axis_length(i_cell) = shape_features(1).MinorAxisLength;
        max_feret_length(i_cell) = shape_features(1).MaxFeretDiameter;
        min_feret_length(i_cell) = shape_features(1).MinFeretDiameter;
        equiv_diameter(i_cell) = shape_features(1).EquivDiameter;
        compactness(i_cell) = (shape_features(1).Perimeter^2) / shape_features(1).Area;
        aspect_ratio(i_cell) = shape_features(1).MaxFeretDiameter / shape_features(1).MinFeretDiameter;
        shape_factor_1(i_cell) = 4* pi * shape_features(1).Area/ shape_features(1).Perimeter^2;
        shape_factor_2(i_cell) = shape_features(1).MaxFeretDiameter/ shape_features(1).Area;
        
        [ROIonly,levels,~,~] = prepareVolume(im_crop,ind_inside_cell,'Other',0.35,3.27,1,1,'Matrix','Equal',8);
        
        GLRLM = getGLRLM(ROIonly,levels);
        glrlmTextures = getGLRLMtextures(GLRLM);
        
        GLSZM = getGLSZM(ROIonly,levels);
        glszmTextures = getGLSZMtextures(GLSZM);
        
        [NGTDM,countValid] = getNGTDM(ROIonly,levels);
        ngtdmTextures = getNGTDMtextures(NGTDM,countValid);
        
        run_length_matrix(i_cell,:) = struct2table(glrlmTextures);
        size_zone_matrix(i_cell,:) = struct2table(glszmTextures);
        gray_tone_difference(i_cell,:) = struct2table(ngtdmTextures);
        
        cell_only = double(im_crop).*ind_inside_cell;
        [invariantMoments] = fRotationInvariantMoments(cell_only);
        invariant_moments(i_cell,:) = array2table(invariantMoments,'VariableNames',invariantNames);
        
        if ~isnan(im_red_crop)
            [ROIonly,levels,ROIbox,maskBox] = prepareVolume(im_red_crop,ind_inside_cell,'Other',0.35,3.27,1,1,'Matrix','Equal',8);
            
            GLRLM = getGLRLM(ROIonly,levels);
            glrlmTextures = getGLRLMtextures(GLRLM);
            
            GLSZM = getGLSZM(ROIonly,levels);
            glszmTextures = getGLSZMtextures(GLSZM);
            
            [NGTDM,countValid] = getNGTDM(ROIonly,levels);
            ngtdmTextures = getNGTDMtextures(NGTDM,countValid);
            
            red_run_length_matrix(i_cell,:) = struct2table(glrlmTextures);
            red_size_zone_matrix(i_cell,:) = struct2table(glszmTextures);
            red_gray_tone_difference(i_cell,:) = struct2table(ngtdmTextures);
            
            [ROIonly,levels,ROIbox,maskBox] = prepareVolume(im_green_crop,ind_inside_cell,'Other',0.35,3.27,1,1,'Matrix','Equal',8);
            
            GLRLM = getGLRLM(ROIonly,levels);
            glrlmTextures = getGLRLMtextures(GLRLM);
            
            GLSZM = getGLSZM(ROIonly,levels);
            glszmTextures = getGLSZMtextures(GLSZM);
            
            [NGTDM,countValid] = getNGTDM(ROIonly,levels);
            ngtdmTextures = getNGTDMtextures(NGTDM,countValid);
            
            green_run_length_matrix(i_cell,:) = struct2table(glrlmTextures);
            green_size_zone_matrix(i_cell,:) = struct2table(glszmTextures);
            green_gray_tone_difference(i_cell,:) = struct2table(ngtdmTextures);
            
                        
            cell_only = double(im_red_crop).*ind_inside_cell;
            [invariantMoments] = fRotationInvariantMoments(cell_only);
            red_invariant_moments(i_cell,:) =  array2table(invariantMoments,'VariableNames',red_invariant_names);
                        
            
            cell_only = double(im_green_crop).*ind_inside_cell;
            [invariantMoments] = fRotationInvariantMoments(cell_only);
            green_invariant_moments(i_cell,:) = array2table(invariantMoments,'VariableNames',green_invariant_names);
        end
        
        
        if ~isempty(feature_cell)
            
            if  isreal(struct2array(feature_cell)) && size(struct2array(feature_cell),2) == 122 %- had some weird problem, fixed this like
                [mitotic_phase score a c]            = predict(Mdl,struct2array(feature_cell));
            else
                mitotic_phase = 5
                c = zeros(1,5);
            end
            
        else
            mitotic_phase = 5
            c = zeros(1,5);
            
            fields = emptyHaralickTable.Properties.VariableNames;
            NaN_temp = NaN(length(fields),1);
            cell_temp = num2cell(NaN_temp);
            feature_cell = cell2struct(cell_temp,fields);
        end
        
        poly_cell      = cell_pos(boundary(cell_pos),:)    ;
        ellipse_struct = fit_ellipse( poly_cell(:,1),poly_cell(:,2) );
        
        
        a_ellipse(i_cell)               = ellipse_struct.a;
        b_ellipse(i_cell)               = ellipse_struct.b;
        X0(i_cell)              = ellipse_struct.X0;
        Y0(i_cell)              = ellipse_struct.Y0;
        X0_in(i_cell)           = ellipse_struct.X0_in;
        Y0_in(i_cell)           = ellipse_struct.Y0_in;
        long_axis(i_cell)       = ellipse_struct.long_axis;
        short_axis(i_cell)      = ellipse_struct.short_axis;
        
        mitotic_phase_store(i_cell)   = mitotic_phase;
        metaphase_probability(i_cell) = c(2);
        debris_probability(i_cell)    = c(4);
        haralick_features(i_cell,:) = struct2table(feature_cell);
        red_haralick_features(i_cell,:) = struct2table(red_feature_cell);
        green_haralick_features(i_cell,:) = struct2table(green_feature_cell);
        
        if metaphase_probability(i_cell) > 0.2
            mitosis_angle(i_cell) = 90 - relative_centroid_angle(i_cell);
            border_mitosis_angle(i_cell) = 90 - relative_border_angle(i_cell);
            if mitosis_angle(i_cell) > 90
                mitosis_angle(i_cell) = 180 - mitosis_angle(i_cell);
            end
            
            
            if border_mitosis_angle(i_cell) > 90
                border_mitosis_angle(i_cell) = 180 - border_mitosis_angle(i_cell);
            end
        else
            mitosis_angle(i_cell) = NaN;
            border_mitosis_angle(i_cell) = NaN;
        end
        
%         catch
%             i_cell
%             'main'
%             continue
%         end
    end
    %%
    voronoi_volume = NaN(size(struct_colony,2),1);
    voronoi_length = zeros(size(struct_colony,2),1);
    voronoi_neighbour_distance =  NaN(size(struct_colony,2),1);
    voronoi_points = cell(size(struct_colony,2),1);
    voronoi_coords = cell(size(struct_colony,2),1);
    [voronoi_vertices, voronoi_cell] = voronoin(centroid_cell);
    voronoi_vertices(~isfinite(voronoi_vertices)) = 0;
    
    % Find voronoi neighbours
    n = length(voronoi_cell);
    vn = false( n, n );
    for voronoi_i = 1 : n
        for voronoi_j = voronoi_i + 1 : n
            s = size ( intersect ( voronoi_cell{voronoi_i}, voronoi_cell{voronoi_j} ) );
            if ( 1 < s(2) )
                vn(voronoi_i,voronoi_j) = 1;
                vn(voronoi_j,voronoi_i) = 1;
            end
        end
    end
    
    voronoi_neighbour_count = sum(vn)';
    for i_cell= 1:length(voronoi_volume)
        try
            X2 = [voronoi_vertices(voronoi_cell{i_cell},1) voronoi_vertices(voronoi_cell{i_cell},2)];
            
            X2(~any(X2,2),:) = [];
            
            neighbour_indices = vn(:,i_cell);
            neighbour_coords = centroid_cell(neighbour_indices,:);
            current_coords = centroid_cell(i_cell,:);
            
            voronoi_neighbour_distance(i_cell) = mean(distmat(current_coords,neighbour_coords));
            if length(X2) < 3 %check for any 0 values which means there is an unbounded polygon, or not enough points for a convhull
                voronoi_volume(i_cell) = NaN;
                voronoi_points{i_cell} = [];
                continue
            end
            [K, V2] = convhull(X2);
            
            point_distances = distmat(X2);
            longest_distance = max(max(point_distances));
            voronoi_length(i_cell) = longest_distance;
            
            
            if voronoi_length(i_cell) < 150
                voronoi_volume(i_cell) = V2;
                voronoi_points{i_cell} = K;
                voronoi_coords{i_cell} = X2;
            else
                voronoi_volume(i_cell) = NaN;
                voronoi_points{i_cell} = [];
                voronoi_coords{i_cell} = [];
                
            end
        
        catch
            i_cell
            'voronoi'
            continue
        end
    end
    
    %% Creation of the single cell table
    
    %%
    
    table_raw_colony_frame = table();
    table_raw_colony_frame.cell_ID    = transpose([struct_colony.ID]);
    table_raw_colony_frame.centroid_X = centroid_cell(:,1);
    table_raw_colony_frame.centroid_Y = centroid_cell(:,2);
    
    
    table_raw_colony_frame.h2b_signal = h2b_signal;
    table_raw_colony_frame.green      = green_signal;
    table_raw_colony_frame.red        = red_signal;      
    table_raw_colony_frame.median_h2b_intensity = median_h2b_intensity;
    table_raw_colony_frame.median_green_intensity = median_green_intensity;
    table_raw_colony_frame.median_red_intensity = median_red_intensity;  
    table_raw_colony_frame.integrated_h2b_intensity = integrated_h2b_intensity;
    table_raw_colony_frame.integrated_green_intensity = integrated_green_intensity;
    table_raw_colony_frame.integrated_red_intensity = integrated_red_intensity;
    table_raw_colony_frame.stdev_h2b_intensity = stdev_h2b_intensity;
    table_raw_colony_frame.stdev_green_intensity  = stdev_green_intensity;
    table_raw_colony_frame.stdev_red_intensity  = stdev_red_intensity;    
    table_raw_colony_frame.mean_green_over_red  = mean_green_over_red;    
    table_raw_colony_frame.mean_h2b_gradient = mean_h2b_gradient;
    table_raw_colony_frame.mean_green_gradient =  mean_green_gradient;
    table_raw_colony_frame.mean_red_gradient =  mean_red_gradient;    
    table_raw_colony_frame.median_h2b_gradient =  median_h2b_gradient;
    table_raw_colony_frame.median_green_gradient =  median_green_gradient;
    table_raw_colony_frame.median_red_gradient =  median_red_gradient;    
    table_raw_colony_frame.stdev_h2b_gradient =  stdev_h2b_gradient;
    table_raw_colony_frame.stdev_green_gradient =  stdev_green_gradient;
    table_raw_colony_frame.stdev_red_gradient =  stdev_red_gradient;    
    table_raw_colony_frame.kurtosis_h2b_gradient =  kurtosis_h2b_gradient;
    table_raw_colony_frame.kurtosis_green_gradient =  kurtosis_green_gradient;
    table_raw_colony_frame.kurtosis_red_gradient =  kurtosis_red_gradient;
    table_raw_colony_frame.frame      = repmat(i_frame,length(green_signal),1);
    table_raw_colony_frame.image      = repmat(fullfile(file_H2B.folder, file_H2B.name),length(centroid_cell(:,1)),1);
    table_raw_colony_frame.col_ID     = repmat(ind_colony(i_colony),length(centroid_cell(:,1)),1);
    table_raw_colony_frame.surface    = {struct_colony.surface}';
    table_raw_colony_frame.area_cell  = area_cell;
    %     table_raw_colony_frame.roundness  = roundness;
    table_raw_colony_frame.distance_centroid_colony   = distance_centroid_colony;
    table_raw_colony_frame.distance_border_colony     = distance_border_colony;
    table_raw_colony_frame.voronoi_volume = voronoi_volume;
    table_raw_colony_frame.voronoi_points = voronoi_points;
    table_raw_colony_frame.voronoi_neighbour_count = voronoi_neighbour_count;
    table_raw_colony_frame.voronoi_neighbour_distance = voronoi_neighbour_distance;
    table_raw_colony_frame.colony_centroid_angle = colony_centroid_difference;
    table_raw_colony_frame.colony_border_angle = colony_border_difference;
    table_raw_colony_frame.line_coords = line_coords;
    table_raw_colony_frame.colony_centroid_pixels = colony_centroid_pixels;
    table_raw_colony_frame.line_params = line_params;
    %     table_raw_colony_frame.line_params_inverse = line_params_inverse;
    table_raw_colony_frame.raw_angle = angle_cell;
    table_raw_colony_frame.relative_centroid_angle = relative_centroid_angle;
    table_raw_colony_frame.relative_border_angle = relative_border_angle;
    table_raw_colony_frame.tan_coords = tan_coords;
    table_raw_colony_frame.tan_params = tan_params;
    table_raw_colony_frame.cent_coords = cent_coords;
    %     table_raw_colony_frame.tan_params_inverse = tan_params_inverse;
    table_raw_colony_frame.skel_length = skel_length;
    table_raw_colony_frame.skel_curve = skel_curve;
    table_raw_colony_frame.mitosis_probability = metaphase_probability;
    table_raw_colony_frame.apoptosis_probability    = debris_probability;
    table_raw_colony_frame.mitosis_angle_to_centroid = mitosis_angle;
    table_raw_colony_frame.mitosis_angle_to_border = border_mitosis_angle;
    table_raw_colony_frame.short_axis = short_axis;
    table_raw_colony_frame.long_axis = long_axis;
    table_raw_colony_frame.a = a_ellipse;
    table_raw_colony_frame.b = b_ellipse;
    table_raw_colony_frame.X0 = X0;
    table_raw_colony_frame.Y0 = Y0;
    table_raw_colony_frame.X0_in = X0_in;
    table_raw_colony_frame.Y0_in = Y0_in;
    table_raw_colony_frame.cell_cycle_phase = mitotic_phase_store;

    table_raw_colony_frame.eccentricity = eccentricity;
    table_raw_colony_frame.solidity = solidity;
%     table_raw_colony_frame.perimeter= perimeter;
    table_raw_colony_frame.major_axis_length = major_axis_length;
    table_raw_colony_frame.minor_axis_length = minor_axis_length;
    table_raw_colony_frame.max_feret_length = max_feret_length;
    table_raw_colony_frame.min_feret_length = min_feret_length;
    table_raw_colony_frame.equiv_diameter = equiv_diameter;
    table_raw_colony_frame.compactness = compactness;
    table_raw_colony_frame.aspect_ratio = aspect_ratio;
    table_raw_colony_frame.shape_factor_1 = shape_factor_1;
    table_raw_colony_frame.shape_factor_2 = shape_factor_2;
    
    table_raw_colony_frame = [table_raw_colony_frame run_length_matrix];
    table_raw_colony_frame = [table_raw_colony_frame size_zone_matrix];
    table_raw_colony_frame = [table_raw_colony_frame gray_tone_difference];
    table_raw_colony_frame = [table_raw_colony_frame red_run_length_matrix];
    table_raw_colony_frame = [table_raw_colony_frame red_size_zone_matrix];
    table_raw_colony_frame = [table_raw_colony_frame red_gray_tone_difference];
    table_raw_colony_frame = [table_raw_colony_frame green_run_length_matrix];
    table_raw_colony_frame = [table_raw_colony_frame green_size_zone_matrix];
    table_raw_colony_frame = [table_raw_colony_frame green_gray_tone_difference];
    table_raw_colony_frame = [table_raw_colony_frame haralick_features];
    table_raw_colony_frame = [table_raw_colony_frame red_haralick_features];
    table_raw_colony_frame = [table_raw_colony_frame green_haralick_features];
    table_raw_colony_frame = [table_raw_colony_frame zernike_features];
    table_raw_colony_frame = [table_raw_colony_frame invariant_moments];
    table_raw_colony_frame = [table_raw_colony_frame red_invariant_moments];
    table_raw_colony_frame = [table_raw_colony_frame green_invariant_moments];

    
  
    %- Additional feature for single cell
    %- Local cell density
    
    
    dum_frame = table_raw_colony_frame;
    
    pos_cell  = [dum_frame.centroid_X dum_frame.centroid_Y];
    distance_cell = distmat(pos_cell);
    
    density_thresholds = [50,60,70,80,90,100,120,140,160,180,200];
    for density_ind = 1:11
          ind_dist  = distance_cell < density_thresholds(density_ind);
          col_name = strcat('density',num2str(density_thresholds(density_ind)));
          density_tab   = table(sum(ind_dist,2));
          density_tab.Properties.VariableNames = {col_name};
          table_raw_colony_frame = [table_raw_colony_frame density_tab];
    end
    
    ksdensity_thresholds = [0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8];
    for ksdensity_ind = 1:length(ksdensity_thresholds)
        [ksdense kscoords] = ksdensity(pos_cell,'Bandwidth',ksdensity_thresholds(ksdensity_ind));
        
        ksdensity_results = NaN(length(pos_cell),1);
        for i_ks = 1:length(pos_cell)
            [closest_coord ~] = dsearchn(kscoords,pos_cell(i_ks,:));
            ksdensity_results(i_ks) = ksdense(closest_coord);
        end
                
        col_name = strcat('ksdensity',num2str(ksdensity_thresholds(ksdensity_ind)));
        col_name = strrep(col_name, '.', '');
        ksdensity_table   = table(ksdensity_results);
        ksdensity_table.Properties.VariableNames = {col_name};
        table_raw_colony_frame = [table_raw_colony_frame ksdensity_table];
    end

    %- Dynamic feature (speed, size change etc..)
    
    
    
    if (~isempty(table_raw_colony_1)) 
        
        
        %         table_raw_colony_1 = table_raw_colony([table_raw_colony.frame] == i_frame-offset_frame,:);
        
        %         table_raw_colony_1 = table_raw_colony([table_raw_colony_frame.frame] == table_raw_colony.frame(find(table_raw_colony.frame < i_frame,1,'last')),:);
%         if i_colony <= length(table_raw_colony_cell)
%             table_raw_colony_1 = table_raw_colony_cell{i_colony};
            
            
            ind_cell      = ismember([table_raw_colony_frame.cell_ID], [table_raw_colony_1.cell_ID]);
            
            
            
            cell_ID_dyn   = table_raw_colony_frame.cell_ID(ind_cell);
            ind_cell_rank = find(ind_cell == 1);
            
            
            dynamic_cell       = NaN(length(ind_cell),1);
            dynamic_cell_size  = NaN(length(ind_cell),1);
            cell_green_change  = NaN(length(ind_cell),1);
            cell_red_change  = NaN(length(ind_cell),1);
            movement_vector = NaN(length(ind_cell),1);
            
            for i_cell = 1:sum(ind_cell)
                
                try
                %--- for the nuclei
                cell_id  = cell_ID_dyn(i_cell);
                
                
                ind_cell_find   = find([table_raw_colony_frame.cell_ID] == cell_id);
                ind_cell_find_1 = find([table_raw_colony_1.cell_ID] == cell_id);
                
                if length(ind_cell_find) > 1
                    ind_cell_find = ind_cell_find(1);
                end
                if length(ind_cell_find_1) > 1
                    ind_cell_find_1 = ind_cell_find_1(1);
                end
                
                
                cell_pos     = [table_raw_colony_frame(ind_cell_find,:).centroid_X table_raw_colony_frame(ind_cell_find,:).centroid_Y];
                cell_pos_1   = [table_raw_colony_1(ind_cell_find_1,:).centroid_X table_raw_colony_1(ind_cell_find_1,:).centroid_Y];
                
                if(cell_pos(1) == 0)
                    continue
                end
                
                initial_colony_centroid_distance = distmat(cell_pos_1,feature_frame.pos_colony);
                new_colony_centroid_distance = distmat(cell_pos,feature_frame.pos_colony);
                
                movement_vector(ind_cell_rank(i_cell))      = initial_colony_centroid_distance - new_colony_centroid_distance;
                
                cell_poly    = table_raw_colony_frame(ind_cell_find,:).surface{1};
                cell_poly_1  = table_raw_colony_1(ind_cell_find_1,:).surface{1};
                
                cell_green   = table_raw_colony_frame(ind_cell_find,:).green;
                cell_green_1 = table_raw_colony_1(ind_cell_find_1,:).green;
                
                
                cell_red   = table_raw_colony_frame(ind_cell_find,:).red;
                cell_red_1 = table_raw_colony_1(ind_cell_find_1,:).red;
                
                area   = polygeom(cell_poly(:,1) ,cell_poly(:,2));
                area_1 = polygeom(cell_poly_1(:,1) ,cell_poly_1(:,2));
                
                area   = area(1);
                area_1 = area_1(1);
                
                cell_green_change(ind_cell_rank(i_cell)) = cell_green - cell_green_1;
                cell_red_change(ind_cell_rank(i_cell)) = cell_red - cell_red_1;
                
                dynamic_cell_size_cell = abs(area - area_1);
                
                distance_cell = distmat(cell_pos, cell_pos_1);
                
                
                
                if size(distance_cell,1) + size(distance_cell,2) >2
                    distance_cell = 0;
                end
                
                dynamic_cell(ind_cell_rank(i_cell))      = distance_cell;
                dynamic_cell_size(ind_cell_rank(i_cell)) = dynamic_cell_size_cell;
                
                
                catch
                    i_cell
                    'track'
                    continue
                end
            end
               
        
    else
        
        movement_vector           = NaN(size(table_raw_colony_frame,1),1);
        dynamic_cell           = NaN(size(table_raw_colony_frame,1),1);
        dynamic_cell_size      = NaN(size(table_raw_colony_frame,1),1);
        cell_green_change  = NaN(size(table_raw_colony_frame,1),1);
        cell_red_change  = NaN(size(table_raw_colony_frame,1),1);
        
        
    end
    
    table_raw_colony_frame.cell_speed      = dynamic_cell;
    table_raw_colony_frame.cell_size_change = dynamic_cell_size;
    table_raw_colony_frame.axial_movement = movement_vector;
    table_raw_colony_frame.axial_movement_direction = sign(movement_vector);
    table_raw_colony_frame.green_change = cell_green_change;
    table_raw_colony_frame.red_change = cell_red_change;
    
%     table_raw_colony_cell{i_colony} = table_raw_colony_frame;
    
    table_raw_cell{i_colony} = table_raw_colony_frame;
% table_raw_colony_store = cat(1,table_raw_colony_store,table_raw_colony_frame);
    



end


table_raw_frame = vertcat(table_raw_cell{:});
table_raw_colony_store = table_raw_frame;
table_raw_frame.image = [];
table_raw_frame.surface = [];
table_raw_frame.voronoi_points = [];
table_raw_frame.line_coords = [];
table_raw_frame.tan_coords = [];
table_raw_frame.colony_centroid_pixels = [];
table_raw_frame.cent_coords = [];



for col = 1:width(table_raw_frame)
    temp_col = table2array(table_raw_frame(:,col));
    temp_col(imag(temp_col) ~= 0) = 0;
    table_raw_frame(:,col) = array2table(temp_col);
end


if ~isfile(strcat(LEVER_STRUCT.write_path,'\single_cell_features_RG5.db'))
    strdb = strcat(LEVER_STRUCT.write_path,'\single_cell_features_RG5.db')
    conn = database(strdb, '','', 'org.sqlite.JDBC', 'jdbc:sqlite:');
    sqlwrite(conn,'single_cell',table_raw_frame)
    
else
    strdb = strcat(LEVER_STRUCT.write_path,'\single_cell_features_RG5.db');
    
    conn = database(strdb, '','', 'org.sqlite.JDBC', 'jdbc:sqlite:');
    sqlwrite(conn,'single_cell',table_raw_frame)

end






end
end
